﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WaterCounter.Models.Entities;

namespace WaterCounter.Repository.DbCore
{
    public interface IRepository<TEnityty>
        where TEnityty : Entity, new()
    {
        IDatabaseContext DatabaseContext { get; }

        Task<List<TEnityty>> GetItemsAsync();

        Task<List<TEnityty>> QueryAsync(string sqlQuery, params object[] args);

        Task<TEnityty> GetItemAsync(int id);

        Task<int> SaveItemAsync(TEnityty item);

        Task<int> UpdateAsync(TEnityty item);

        Task<int> DeleteItemAsync(TEnityty item);
    }
}