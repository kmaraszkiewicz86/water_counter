﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WaterCounter.Models.Entities;

namespace WaterCounter.Repository.DbCore
{
    public class DatabaseCreator : IDatabaseCreator
    {
        private readonly IDatabaseContext _databaseContext;

        public DatabaseCreator(IDatabaseContext databaseContext)
        {
            _databaseContext = databaseContext;
        }

        public async Task InitializeDatabaseAsync()
        {
            await _databaseContext.Database.CreateTableAsync<CupType>();
            await _databaseContext.Database.CreateTableAsync<DrankWaterStatus>();
        }
    }
}