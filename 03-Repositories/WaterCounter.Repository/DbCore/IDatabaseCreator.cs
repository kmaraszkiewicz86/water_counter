﻿using System.Threading.Tasks;

namespace WaterCounter.Repository.DbCore
{
    public interface IDatabaseCreator
    {
        Task InitializeDatabaseAsync();
    }
}