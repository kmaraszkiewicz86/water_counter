﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SQLite;
using WaterCounter.Models.Entities;

namespace WaterCounter.Repository.DbCore
{
    public class Repository<TEnityty> : IRepository<TEnityty>
        where TEnityty : Entity, new()
    {
        public IDatabaseContext DatabaseContext { get; }

        public Repository(IDatabaseContext databaseContext)
        {
            DatabaseContext = databaseContext;
        }

        public async Task<List<TEnityty>> GetItemsAsync()
        {
            return await DatabaseContext.Database.Table<TEnityty>().ToListAsync();
        }

        public async Task<List<TEnityty>> QueryAsync(string sqlQuery, params object[] args)
        {
            return await DatabaseContext.Database.QueryAsync<TEnityty>(sqlQuery, args);
        }

        public async Task<TEnityty> GetItemAsync(int id)
        {
            return await DatabaseContext.Database.Table<TEnityty>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public AsyncTableQuery<TEnityty> GetEnitytyTable()
        {
            return DatabaseContext.Database.Table<TEnityty>();
        }

        public async Task<int> SaveItemAsync(TEnityty item)
        {
            return await DatabaseContext.Database.InsertAsync(item);
        }

        public async Task<int> UpdateAsync(TEnityty item)
        {
            return await DatabaseContext.Database.UpdateAsync(item);
        }

        public async Task<int> DeleteItemAsync(TEnityty item)
        {
            return await DatabaseContext.Database.DeleteAsync(item);
        }
    }
}