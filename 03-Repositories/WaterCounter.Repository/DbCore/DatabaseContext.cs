﻿using System;
using System.Diagnostics;
using SQLite;

namespace WaterCounter.Repository.DbCore
{
    public class DatabaseContext : IDatabaseContext
    {
        public const SQLite.SQLiteOpenFlags Flags =
            // open the database in read/write mode
            SQLite.SQLiteOpenFlags.ReadWrite |
            // create the database if it doesn't exist
            SQLite.SQLiteOpenFlags.Create |
            // enable multi-threaded database access
            SQLite.SQLiteOpenFlags.SharedCache;

        public SQLiteAsyncConnection Database { get; }

        public DatabaseContext(string localApplicationDataPath)
        {
            Database = new SQLiteAsyncConnection(localApplicationDataPath, Flags);
            Database.Tracer = new Action<string>(q => Debug.WriteLine(q));
            Database.Trace = true;
        }

        ~DatabaseContext()
        {
            Database?.CloseAsync();
        }
    }
}