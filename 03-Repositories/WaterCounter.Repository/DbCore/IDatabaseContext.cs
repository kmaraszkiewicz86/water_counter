﻿using System.Threading.Tasks;
using SQLite;

namespace WaterCounter.Repository.DbCore
{
    public interface IDatabaseContext
    {
        SQLiteAsyncConnection Database { get; }
    }
}