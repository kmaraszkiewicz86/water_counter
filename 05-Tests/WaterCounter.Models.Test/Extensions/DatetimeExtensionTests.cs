﻿using System;
using NUnit.Framework;
using WaterCounter.Models.Extensions;

namespace WaterCounter.Models.Test.Extensions
{
    public class DatetimeExtensionTests
    {
        [Test]
        public void IsDateToday_WhenCurrentDateIsToday_ThenReturnTrue()
        {
            var currentDate = DateTime.Now;
            var testedDate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, 0, 0, 0);

            Assert.IsTrue(testedDate.IsDateToday());
        }

        [Test]
        public void IsDateToday_WhenCurrentDateIsToday_ThenReturnFalse()
        {
            var currentDate = DateTime.Now.AddDays(-1);
            var testedDate = new DateTime(currentDate.Year, currentDate.Month, currentDate.Day, 23, 59, 59);

            Assert.IsFalse(testedDate.AddDays(-1).IsDateToday());
        }

        [Test]
        public void IsNewReportDateGreaterThenToday_WhenDateIsGreatherThenToday_ThenReturnTrue()
        {
            Assert.IsTrue(DateTime.Now.AddDays(1).IsNewReportDateGreaterThenToday());
        }

        [Test]
        public void IsNewReportDateGreaterThenToday_WhenDateEqualsToday_ThenReturnFalse()
        {
            Assert.IsFalse(DateTime.Now.IsNewReportDateGreaterThenToday());
        }

        [Test]
        public void IsNewReportHasLessThanThirtyDaysBack_WhenDateIsLowerThanThirtyDaysBack_ThenReturnTrue()
        {
            Assert.IsTrue(DateTime.Now.AddDays(-31).IsNewReportHasLessThanThirtyDaysBack());
        }

        [Test]
        public void IsNewReportHasLessThanThirtyDaysBack_WhenDateIsGreatherThanThirtyDaysBack_ThenReturnFalse()
        {
            Assert.IsFalse(DateTime.Now.AddDays(-30).IsNewReportHasLessThanThirtyDaysBack());
        }

        [Test]
        public void ReturnValidDate_WhenDateIsGreatherThenToday_ThenReturnTodayDate()
        {
            EqualsTwoDate(DateTime.Now.AddDays(1).ReturnValidDate(), DateTime.Now);
        }

        [Test]
        public void ReturnValidDate_WhenDateEqualsToday_ThenReturnFalse()
        {
            EqualsTwoDate(DateTime.Now.ReturnValidDate(), DateTime.Now);
        }

        [Test]
        public void ReturnValidDate_WhenDateIsLowerThanThirtyDaysBack_ThenReturnTrue()
        {
            EqualsTwoDate(DateTime.Now.AddDays(-31).ReturnValidDate(), DateTime.Now.AddDays(-30));
        }

        [Test]
        public void ReturnValidDate_WhenDateIsGreatherThanThirtyDaysBack_ThenReturnFalse()
        {
            EqualsTwoDate(DateTime.Now.AddDays(-30).ReturnValidDate(), DateTime.Now.AddDays(-30));
        }

        private void EqualsTwoDate(DateTime date1, DateTime date2)
        {
            if (date1.Year != date2.Year
                || date1.Month != date2.Month
                || date1.Day != date2.Day)
            {
                throw new Exception($"{date1:yyyy-MM-dd} is not equal {date2:yyyy-MM-dd}");
            }
        }
    }
}