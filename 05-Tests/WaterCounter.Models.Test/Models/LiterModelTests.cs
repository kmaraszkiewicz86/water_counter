﻿using System;
using NUnit.Framework;
using WaterCounter.Models.Models;

namespace WaterCounter.Models.Test.Models
{
    public class LiterModelTests
    {
        private LiterModel literModel;

        [SetUp]
        public void SetUp()
        {
            literModel = new LiterModel(5, 200);
        }

        [TestCase(0, 5, 200)]
        [TestCase(200, 5, 0)]
        [TestCase(250, 4, 950)]
        [TestCase(2500, 2, 700)]
        [TestCase(4200, 1, 0)]
        [TestCase(5100, 0, 100)]
        [TestCase(5300, 0, 0)]
        public void WhenUserSubstractMiliterFromModel_ThenCorrectSubstringHasToMade(
            int valueToSubstract,
            int expectedLiters,
            int expectedMiliters)
        {
            literModel.Substract(valueToSubstract);

            AssertResult(expectedLiters, expectedMiliters);
        }

        [TestCase(0, 5, 200)]
        [TestCase(250, 5, 450)]
        [TestCase(1810, 7, 10)]
        public void WhenUserAddMiliterFromModel_ThenCorrectSubstringHasToMade(
            int valueToAdd,
            int expectedLiters,
            int expectedMiliters)
        {
            literModel.Add(valueToAdd);

            AssertResult(expectedLiters, expectedMiliters);
        }

        private void AssertResult(int expectedLiters, int expectedMiliters)
        {
            Assert.AreEqual(literModel.Liters, expectedLiters);
            Assert.AreEqual(literModel.Mililiters, expectedMiliters);
        }
    }
}
