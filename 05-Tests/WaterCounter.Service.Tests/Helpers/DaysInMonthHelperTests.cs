﻿using System;
using FluentAssertions;
using NUnit.Framework;
using WaterCounter.Service.Helpers;

namespace WaterCounter.Service.Tests.Helpers
{
    public class DaysInMonthHelperTests
    {
        [TestCase(1, ExpectedResult = 31)]
        [TestCase(3, ExpectedResult = 31)]
        [TestCase(4, ExpectedResult = 30)]
        [TestCase(5, ExpectedResult = 31)]
        [TestCase(6, ExpectedResult = 30)]
        [TestCase(7, ExpectedResult = 31)]
        [TestCase(8, ExpectedResult = 31)]
        [TestCase(9, ExpectedResult = 30)]
        [TestCase(10, ExpectedResult = 31)]
        [TestCase(11, ExpectedResult = 30)]
        [TestCase(12, ExpectedResult = 31)]
        public int GetDaysOfMonth_ShouldReturnCorrectDays_RelatedToTypedMonth(int month)
        {
            return DaysInMonthHelper.GetDaysOfMonth(new DateTime(2021, month, 1));
        }

        [TestCase(2020, ExpectedResult = 29)]
        [TestCase(2019, ExpectedResult = 28)]
        public int GetDaysOfFebuary_ShouldReturnCorrectDays_RelatedToTypedMonth(int year)
        {
            return DaysInMonthHelper.GetDaysOfMonth(new DateTime(year, 2, 1));
        }
    }
}