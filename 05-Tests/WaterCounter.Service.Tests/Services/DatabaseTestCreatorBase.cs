﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using WaterCounter.Models.Entities;
using WaterCounter.Repository.DbCore;
using SQLite;
using System.Linq;

namespace WaterCounter.Service.Tests.Services
{
    internal abstract class DatabaseTestCreatorBase
    {
        private string TestDatabasePath
        {
            get
            {
                var directoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                return Path.Combine(directoryPath, "water_counter_test.db3");
            }
        }

        private DatabaseContext _databaseContext;

        protected async Task InitializeDatabaseAsync()
        {
            _databaseContext = new DatabaseContext(TestDatabasePath);

            await new DatabaseCreator(_databaseContext).InitializeDatabaseAsync();

            await ClearDataInDatabase();
        }

        protected IRepository<TEntity> GetRepository<TEntity>()
            where TEntity : Entity, new()
        {
            return new Repository<TEntity>(_databaseContext);
        }

        protected async Task AddTestItems<TEntity>(IEnumerable<TEntity> entities)
            where TEntity : Entity, new()
        {
            var repository = GetRepository<TEntity>();

            foreach (var entity in entities)
            {
                await repository.SaveItemAsync(entity);
            }
        }

        protected async Task ClearDataInDatabase()
        {
            await ClearItemsFromTable<CupType>();
            await ClearItemsFromTable<DrankWaterStatus>();
        }

        private async Task ClearItemsFromTable<TEnitity>()
            where TEnitity : Entity, new()
        {
            var tableNameAttribute = (TableAttribute)typeof(TEnitity)
                .GetCustomAttributes(typeof(TableAttribute))
                .FirstOrDefault();

            var tableName = typeof(TEnitity).Name;

            if (tableNameAttribute != null)
            {
                tableName = tableNameAttribute.Name;
            }

            var repository = GetRepository<TEnitity>();

            await repository.QueryAsync($"DELETE FROM {tableName}");
            await repository.QueryAsync($"DELETE FROM SQLITE_SEQUENCE WHERE name=?;", tableName);
        }
    }
}