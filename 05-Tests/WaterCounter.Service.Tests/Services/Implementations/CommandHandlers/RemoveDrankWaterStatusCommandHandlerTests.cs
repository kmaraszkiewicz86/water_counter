﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Models;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Services.Implementations.CommandHandlers;
using WaterCounter.Service.Services.Implementations.Commands;

namespace WaterCounter.Service.Services.Tests.Implementations.CommandHandlers
{
    public class RemoveDrankWaterStatusCommandHandlerTests
    {
        private RemoveDrankWaterStatusCommandHandlerAsync _serviceUnderTests;

        private Mock<IRepository<DrankWaterStatus>> repositoryMock;

        [SetUp]
        public void SetUp()
        {
            repositoryMock = new Mock<IRepository<DrankWaterStatus>>();

            _serviceUnderTests = new RemoveDrankWaterStatusCommandHandlerAsync(repositoryMock.Object);
        }

        [Test]
        public async Task ExecuteAsync_UserAddNewItem_Then_ResultShouldHasVAlidState()
        {
            var model = new RemoveDranlWaterStatusItemCommand(2);

            repositoryMock.Setup(mock => mock.GetItemAsync(It.IsAny<int>()))
                .Returns(Task.FromResult(new DrankWaterStatus { Id = 1, CupTypeId = 1, AddedDate = DateTime.Now }));

            Result result = await _serviceUnderTests.ExecuteAsync(model);

            result.IsValid.Should().BeTrue();

            repositoryMock.Verify(mock => mock.DeleteItemAsync(It.IsAny<DrankWaterStatus>()), Times.Once);
        }

        [Test]
        public async Task ExecuteAsync_UserAddNewItemWithInvalidId_Then_ResultShouldHasInvalidState()
        {
            var model = new RemoveDranlWaterStatusItemCommand(7);

            repositoryMock.Setup(mock => mock.GetItemAsync(It.IsAny<int>()))
                .Returns(Task.FromResult<DrankWaterStatus>(null));

            Result result = await _serviceUnderTests.ExecuteAsync(model);

            result.IsValid.Should().BeFalse();

            repositoryMock.Verify(mock => mock.DeleteItemAsync(It.IsAny<DrankWaterStatus>()), Times.Never);
        }
    }
}