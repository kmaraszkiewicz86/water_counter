﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Models;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Services.Implementations.CommandHandlers;
using WaterCounter.Service.Services.Implementations.Commands;
using WaterCounter.Service.Tests.Services;
using WaterCounter.Service.Tests.TestHelpers;

namespace WaterCounter.Service.Services.Tests.Implementations.CommandHandlers
{
    internal class CupTypeCommandHandlerTests : DatabaseTestCreatorBase
    {
        private CupTypeCommandHandlerAsync _serviceUnderTests;

        private IRepository<CupType> _cupTypeRepository;

        [SetUp]
        public async Task SetUpAsync()
        {
            var cupTypes = new List<CupType>
            {
                new CupType{ Id = 1, Name = "Item1",  Value = 1 }
            };

            await InitializeDatabaseAsync();

            await AddTestItems(cupTypes);

            _cupTypeRepository = GetRepository<CupType>();

            _serviceUnderTests = new CupTypeCommandHandlerAsync(_cupTypeRepository);
        }

        [Test]
        public async Task ExecuteAsync_UserAddNewItem_Then_ResultShouldHasValidState()
        {
            AddCupTypeCommand addCupTypeCommand = new AddCupTypeCommand(new List<CupTypeModel>
            {
                new CupTypeModel("Item3", 2),
                new CupTypeModel("Item1", 1),
                new CupTypeModel("", 1),
                new CupTypeModel(" ", 1)
            });

            Result result = await _serviceUnderTests.ExecuteAsync(addCupTypeCommand);

            List<CupType> cupTypesFromDatabase = await _cupTypeRepository.GetItemsAsync();

            cupTypesFromDatabase.Count.Should().Be(2);

            cupTypesFromDatabase = cupTypesFromDatabase.OrderBy(c => c.Name).ToList();

            cupTypesFromDatabase.First().Name.Should().Be("Item1");
            cupTypesFromDatabase.Last().Name.Should().Be("Item3");

            result.IsValid.Should().BeTrue();
        }
    }
}