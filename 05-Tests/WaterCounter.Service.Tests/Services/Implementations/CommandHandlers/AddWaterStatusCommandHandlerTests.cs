﻿using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Models;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Services.Implementations.CommandHandlers;
using WaterCounter.Service.Services.Implementations.Commands;
using WaterCounter.Service.Tests.Services;
using static WaterCounter.Service.Tests.TestData.CupTypeDataInitializer;

namespace WaterCounter.Service.Services.Tests.Implementations.CommandHandlers
{
    internal class AddDrankWaterStatusCommandHandlerTests : DatabaseTestCreatorBase
    {
        private AddDrankWaterStatusCommandHandlerAsync _serviceUnderTests;

        private IRepository<CupType> _cupTypeRepository;

        private IRepository<DrankWaterStatus> _drankWaterStatusRepository;

        [SetUp]
        public async Task SetUpAsync()
        {
            await InitializeDatabaseAsync();

            await AddTestItems(CupTypes);

            _cupTypeRepository = GetRepository<CupType>();
            _drankWaterStatusRepository = GetRepository<DrankWaterStatus>();

            _serviceUnderTests = new AddDrankWaterStatusCommandHandlerAsync(_cupTypeRepository,
                _drankWaterStatusRepository);
        }

        private async Task SetUpResultExpectetion(bool shouldCheckValidResult)
        {
            List<DrankWaterStatus> drankWaterStatuses = await _drankWaterStatusRepository.GetItemsAsync();

            drankWaterStatuses.Count.Should().Be(shouldCheckValidResult ? 1 : 0);
        }

        [Test]
        public async Task ExecuteAsync_UserAddNewItem_Then_ResultShouldHasValidState()
        {
            var model = new AddDrankWaterStatusCommand(new CupTypeModel(1, "250 ml", 250), DateTime.Now);

            Result result = await _serviceUnderTests.ExecuteAsync(model);

            result.IsValid.Should().BeTrue();

            await SetUpResultExpectetion(true);
        }

        [Test]
        public async Task ExecuteAsync_UserAddNewItemWithInvalidId_Then_ResultShouldHasInvalidState()
        {
            var model = new AddDrankWaterStatusCommand(new CupTypeModel(0, "250 ml", 250), DateTime.Now);

            Result result = await _serviceUnderTests.ExecuteAsync(model);

            result.IsValid.Should().BeFalse();
            result.ErrorMessage.Should().Be("Select cup type before continue...");

            await SetUpResultExpectetion(false);
        }

        [Test]
        public async Task ExecuteAsync_UserAddNullItem_Then_ResultShouldHasInvalidState()
        {
            Result result = await _serviceUnderTests.ExecuteAsync(null);

            result.IsValid.Should().BeFalse();
            result.ErrorMessage.Should().Be("Select cup type before continue...");

            await SetUpResultExpectetion(false);
        }

        [Test]
        public async Task ExecuteAsync_UserSelectCupTypeThatNotExists_Then_ResultShouldHasInvalidState()
        {
            var model = new AddDrankWaterStatusCommand(new CupTypeModel(10, "2500 ml", 250), DateTime.Now);

            Result result = await _serviceUnderTests.ExecuteAsync(model);

            result.IsValid.Should().BeFalse();
            result.ErrorMessage.Should().Be("CupType with id 10 not found");

            await SetUpResultExpectetion(false);
        }
    }
}