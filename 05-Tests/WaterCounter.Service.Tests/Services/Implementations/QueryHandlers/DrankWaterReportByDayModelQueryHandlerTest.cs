﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Implementations.QueryHandlers;
using static WaterCounter.Service.Tests.TestData.CupTypeDataInitializer;

namespace WaterCounter.Service.Tests.Services.Implementations.QueryHandlers
{
    internal class DrankWaterReportByDayModelQueryHandlerTest : DatabaseTestCreatorBase
    {
        private DrankWaterReportByDayModelQueryHandler _serviceUnderTests;

        private DateTime[] dateTimes => new DateTime[]
        {
            GetFirstDayOfMonthInDatetime(DateTime.UtcNow),
            GetFirstDayOfMonthInDatetime(DateTime.UtcNow.AddDays(-1)),
            GetFirstDayOfMonthInDatetime(DateTime.UtcNow.AddDays(-20)),
            GetFirstDayOfMonthInDatetime(DateTime.UtcNow.AddDays(-31)),
            GetFirstDayOfMonthInDatetime(DateTime.UtcNow.AddDays(-32))
        };

        private List<DrankWaterStatus> DrankWaterStatuses
        {
            get
            {
                return new List<DrankWaterStatus>
                {
                        new DrankWaterStatus { Id = 1, CupTypeId = 1, AddedDate = dateTimes[0] },
                        new DrankWaterStatus { Id = 2, CupTypeId = 1, AddedDate = dateTimes[0] },
                        new DrankWaterStatus { Id = 3, CupTypeId = 2, AddedDate = dateTimes[0] },

                        new DrankWaterStatus { Id = 4, CupTypeId = 2, AddedDate = dateTimes[1] },
                        new DrankWaterStatus { Id = 5, CupTypeId = 2, AddedDate = dateTimes[1] },
                        new DrankWaterStatus { Id = 6, CupTypeId = 2, AddedDate = dateTimes[1] },
                        new DrankWaterStatus { Id = 7, CupTypeId = 2, AddedDate = dateTimes[1] },
                        new DrankWaterStatus { Id = 8, CupTypeId = 1, AddedDate = dateTimes[1] },

                        new DrankWaterStatus { Id = 9, CupTypeId = 2, AddedDate = dateTimes[2] },
                        new DrankWaterStatus { Id = 10, CupTypeId = 2, AddedDate = dateTimes[2] },
                        new DrankWaterStatus { Id = 11, CupTypeId = 2, AddedDate = dateTimes[2] },
                        new DrankWaterStatus { Id = 12, CupTypeId = 2, AddedDate = dateTimes[2] },
                        new DrankWaterStatus { Id = 13, CupTypeId = 2, AddedDate = dateTimes[2] },
                        new DrankWaterStatus { Id = 14, CupTypeId = 1, AddedDate = dateTimes[2] },

                        new DrankWaterStatus { Id = 15, CupTypeId = 2, AddedDate = dateTimes[3] },
                        new DrankWaterStatus { Id = 16, CupTypeId = 1, AddedDate = dateTimes[3] },

                        new DrankWaterStatus { Id = 17, CupTypeId = 1, AddedDate = dateTimes[4] },
                };
            }
        }

        [SetUp]
        public async Task SetUp()
        {
            await InitializeDatabaseAsync();

            await AddTestItems(CupTypes);
            await AddTestItems(DrankWaterStatuses);

            _serviceUnderTests = new DrankWaterReportByDayModelQueryHandler(GetRepository<DrankWaterStatus>());
        }

        [Test]
        public async Task HandleAsync_GenerateValidReportByDay()
        {
            ResultWithModel<IEnumerable<DrankWaterReportByDayModel>> result = await _serviceUnderTests.HandleAsync();

            result.IsValid.Should().BeTrue();

            result.Model.Count().Should().Be(31);

            List<DrankWaterReportByDayModel> drankWaterReportByDayModels = result.Model.ToList();

            for (var index = 0; index < drankWaterReportByDayModels.Count; index++)
            {
                var modelFromSpecificDay = drankWaterReportByDayModels[index];

                switch (index)
                {
                    case 0:
                        modelFromSpecificDay.DrankWaterInMililiters.Should().Be(2250);
                        break;

                    case 19:
                        modelFromSpecificDay.DrankWaterInMililiters.Should().Be(2750);
                        break;

                    case 30:
                        modelFromSpecificDay.DrankWaterInMililiters.Should().Be(750);
                        break;

                    default:
                        modelFromSpecificDay.DrankWaterInMililiters.Should().Be(0);
                        break;
                }

            }
        }

        private DateTime GetFirstDayOfMonthInDatetime(DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, dateTime.Day);
        }
    }
}