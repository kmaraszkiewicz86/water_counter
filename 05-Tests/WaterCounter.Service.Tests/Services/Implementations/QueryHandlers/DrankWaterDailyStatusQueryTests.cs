﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Implementations.Queries;
using WaterCounter.Service.Services.Implementations.QueryHandlers;
using static WaterCounter.Service.Tests.TestData.CupTypeDataInitializer;

namespace WaterCounter.Service.Tests.Services.Implementations.QueryHandlers
{
    internal class DrankWaterDailyStatusQueryHandlerTests : DatabaseTestCreatorBase
    {
        private DrankWaterDailyStatusQueryHandler _serviceUnderTests;

        private List<DrankWaterStatus> DrankWaterStatuses => new List<DrankWaterStatus>
        {
                new DrankWaterStatus { Id = 1, CupTypeId = 1, AddedDate = new DateTime(2020, 01, 01, 0, 0, 0) },
                new DrankWaterStatus { Id = 2, CupTypeId = 1, AddedDate = new DateTime(2020, 12, 23, 20, 0, 0) },
                new DrankWaterStatus { Id = 3, CupTypeId = 1, AddedDate = new DateTime(2020, 12, 31, 23, 59, 59) },

                new DrankWaterStatus { Id = 4, CupTypeId = 2, AddedDate = new DateTime(2021, 01, 01, 0, 0, 5) },
                new DrankWaterStatus { Id = 5, CupTypeId = 1, AddedDate = new DateTime(2021, 01, 01, 1, 1, 1) },
                new DrankWaterStatus { Id = 6, CupTypeId = 2, AddedDate = new DateTime(2021, 01, 01, 15, 25, 36) },
                new DrankWaterStatus { Id = 7, CupTypeId = 1, AddedDate = new DateTime(2021, 01, 01, 9, 11, 12) },
                new DrankWaterStatus { Id = 8, CupTypeId = 1, AddedDate = new DateTime(2021, 01, 01, 18, 12, 0) },
                new DrankWaterStatus { Id = 9, CupTypeId = 1, AddedDate = new DateTime(2021, 01, 01, 19, 54, 15) },
                new DrankWaterStatus { Id = 9, CupTypeId = 2, AddedDate = new DateTime(2021, 01, 01, 23, 59, 59) },

                new DrankWaterStatus { Id = 11, CupTypeId = 2, AddedDate = new DateTime(2021, 01, 02, 00, 00, 00) },
                new DrankWaterStatus { Id = 12, CupTypeId = 1, AddedDate = new DateTime(2021, 01, 02, 23, 59, 59) },
        };

        [SetUp]
        public async Task SetUp()
        {
            await InitializeDatabaseAsync();

            await AddTestItems(CupTypes);
            await AddTestItems(DrankWaterStatuses);

            _serviceUnderTests = new DrankWaterDailyStatusQueryHandler(GetRepository<CupType>(),
                GetRepository<DrankWaterStatus>());
        }

        [Test]
        public async Task HandleAsync_GenerateValidReportFromDayThatChoseUser()
        {
            int expectedBiggerCupCount = 3;
            int expectedSmallerCupCount = 4;
            int expectedValueInMiliLiters = 2500;
            LiterModel expectedLiterModel = new LiterModel(2, 500);

            DrankWaterDailyStatusModelCollection result = (await _serviceUnderTests.HandleAsync(new DrankWaterDailyStatusQuery(new DateTime(2021, 1, 1)))).Model;

            result.DrankWaterStatusReportModels.Should().NotBeNullOrEmpty();
            result.DrankWaterStatusReportModels.Count().Should().Be(7);

            foreach (DrankWaterStatusReportModel drankWaterStatusReportModels in result.DrankWaterStatusReportModels)
            {
                if (drankWaterStatusReportModels.CupTypeModel.Value == 500)
                {
                    expectedBiggerCupCount--;
                }
                else if (drankWaterStatusReportModels.CupTypeModel.Value == 250)
                {
                    expectedSmallerCupCount--;
                }
            }

            expectedSmallerCupCount.Should().Be(0);
            expectedBiggerCupCount.Should().Be(0);

            result.CupTypeValuesSummaryInMililiters.Should().Be(expectedValueInMiliLiters);
            result.LiterModelSummary.Should().BeEquivalentTo(expectedLiterModel);
        }
    }
}