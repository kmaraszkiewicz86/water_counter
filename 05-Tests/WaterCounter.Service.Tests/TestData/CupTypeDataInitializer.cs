﻿using System.Collections.Generic;
using WaterCounter.Models.Entities;

namespace WaterCounter.Service.Tests.TestData
{
    public static class CupTypeDataInitializer
    {
        public static List<CupType> CupTypes => new List<CupType>
        {
            new CupType
            {
                Id = 1,
                Name = "250 ml",
                Value = 250
            },
            new CupType
            {
                Id = 2,
                Name = "500 ml",
                Value = 500
            }
        };
    }
}