﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Moq;
using WaterCounter.Models.Entities;
using WaterCounter.Repository.DbCore;

namespace WaterCounter.Service.Tests.TestHelpers
{
    public class CupTypeRepositoryFake : IRepository<CupType>
    {
        public IDatabaseContext DatabaseContext { get; }

        public int SaveItemAsyncRunTimes { get; private set; } = 0;

        private List<CupType> cupTypes => new List<CupType>
        {
            new CupType
            {
                Id = 1,
                Name = "250 ml",
                Value = 250
            },
            new CupType
            {
                Id = 2,
                Name = "500 ml",
                Value = 500
            }
        };

        public CupTypeRepositoryFake()
        {
            var databaseContext = new Mock<IDatabaseContext>();
            databaseContext.SetupGet(mock => mock.Database).Returns(new SQLiteAsyncConnectionFake("test"));

            DatabaseContext = databaseContext.Object;
        }

        public Task<int> DeleteItemAsync(CupType item)
        {
            throw new System.NotImplementedException();
        }

        public Task<CupType> GetItemAsync(int id)
        {
            return Task.FromResult(cupTypes.FirstOrDefault(i => i.Id == id));
        }

        public Task<List<CupType>> GetItemsAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task<List<CupType>> QueryAsync(string sqlQuery, params object[] args)
        {
            if (args[0].ToString() == "item1")
            {
                return Task.FromResult(new List<CupType>
                {
                    new CupType { Id = 1, Name = "250 ml", Value = 250 }
                });
            }

            return Task.FromResult(new List<CupType>
            {
            });
        }

        public Task<int> SaveItemAsync(CupType item)
        {
            SaveItemAsyncRunTimes++;
            return Task.FromResult(1);
        }

        public Task<int> UpdateAsync(CupType item)
        {
            return Task.FromResult(1);
        }
    }
}