﻿using System;
using System.Threading.Tasks;
using SQLite;

namespace WaterCounter.Service.Tests.TestHelpers
{
    public class SQLiteAsyncConnectionFake : SQLiteAsyncConnection
    {
        public SQLiteAsyncConnectionFake() : this("test")
        {

        }

        public SQLiteAsyncConnectionFake(SQLiteConnectionString connectionString) : base(connectionString)
        {
        }

        public SQLiteAsyncConnectionFake(string databasePath, bool storeDateTimeAsTicks = true) : base(databasePath, storeDateTimeAsTicks)
        {
        }

        public SQLiteAsyncConnectionFake(string databasePath, SQLiteOpenFlags openFlags, bool storeDateTimeAsTicks = true) : base(databasePath, openFlags, storeDateTimeAsTicks)
        {
        }

        public new Task RunInTransactionAsync(Action<SQLiteConnection> action)
        {
            action?.Invoke(new SQLiteConnectionFake());

            return Task.CompletedTask;
        }
    }
}