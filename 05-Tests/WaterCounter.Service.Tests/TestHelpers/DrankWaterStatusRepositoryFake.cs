﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WaterCounter.Models.Entities;
using WaterCounter.Repository.DbCore;

namespace WaterCounter.Service.Tests.TestHelpers
{
    public class DrankWaterStatusRepositoryFake : IRepository<DrankWaterStatus>
    {
        public IDatabaseContext DatabaseContext { get; }

        public int SaveItemAsyncRunTimes { get; private set; } = 0;

        public Task<int> DeleteItemAsync(DrankWaterStatus item)
        {
            throw new System.NotImplementedException();
        }

        public Task<DrankWaterStatus> GetItemAsync(int id)
        {
            throw new System.NotImplementedException();
        }

        public Task<List<DrankWaterStatus>> GetItemsAsync()
        {
            throw new System.NotImplementedException();
        }

        public Task<List<DrankWaterStatus>> QueryAsync(string sqlQuery, params object[] args)
        {
            return Task.FromResult(new List<DrankWaterStatus>
            {
                new DrankWaterStatus { Id = 1, CupTypeId = 1, AddedDate = new DateTime(2021, 01, 01, 1, 1, 1) },
                new DrankWaterStatus { Id = 2, CupTypeId = 2, AddedDate = new DateTime(2021, 01, 01, 0, 0, 5) },
                new DrankWaterStatus { Id = 3, CupTypeId = 2, AddedDate = new DateTime(2021, 01, 01, 15, 25, 36) },
                new DrankWaterStatus { Id = 4, CupTypeId = 1, AddedDate = new DateTime(2021, 01, 01, 9, 11, 12) },
                new DrankWaterStatus { Id = 5, CupTypeId = 1, AddedDate = new DateTime(2021, 01, 01, 18, 12, 0) },
                new DrankWaterStatus { Id = 6, CupTypeId = 1, AddedDate = new DateTime(2021, 01, 01, 19, 54, 15) },
                new DrankWaterStatus { Id = 7, CupTypeId = 2, AddedDate = new DateTime(2021, 01, 01, 0, 0, 1) },
                new DrankWaterStatus { Id = 8, CupTypeId = 1, AddedDate = new DateTime(2021, 01, 01, 0, 0, 0) },
                new DrankWaterStatus { Id = 9, CupTypeId = 2, AddedDate = new DateTime(2021, 01, 01, 23, 59, 59) }
            });
        }

        public Task<int> SaveItemAsync(DrankWaterStatus item)
        {
            return Task.FromResult(1);
        }

        public Task<int> UpdateAsync(DrankWaterStatus item)
        {
            return Task.FromResult(1);
        }
    }
}