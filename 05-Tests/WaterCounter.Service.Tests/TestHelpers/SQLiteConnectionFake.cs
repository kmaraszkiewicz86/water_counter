﻿using SQLite;

namespace WaterCounter.Service.Tests.TestHelpers
{
    public class SQLiteConnectionFake : SQLiteConnection
    {
        public SQLiteConnectionFake() : this("test", true)
        {

        }

        public SQLiteConnectionFake(SQLiteConnectionString connectionString) : base(connectionString)
        {
        }

        public SQLiteConnectionFake(string databasePath, bool storeDateTimeAsTicks = true) : base(databasePath, storeDateTimeAsTicks)
        {
        }

        public SQLiteConnectionFake(string databasePath, SQLiteOpenFlags openFlags, bool storeDateTimeAsTicks = true) : base(databasePath, openFlags, storeDateTimeAsTicks)
        {
        }

        public new void Commit()
        {

        }
    }
}
