﻿using System;
using Xamarin.Forms;

namespace WaterCounter.Tests.TestHelpers
{
    public class DispatcherTest : IDispatcher
    {
        public bool IsInvokeRequired => true;

        public void BeginInvokeOnMainThread(Action action)
        {
            action?.Invoke();
        }
    }
}