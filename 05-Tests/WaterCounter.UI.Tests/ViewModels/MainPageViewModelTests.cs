﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using WaterCounter.Models.Models;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Services.Implementations.Commands;
using WaterCounter.Service.Services.Implementations.Queries;
using WaterCounter.Service.Services.Interfaces.Decorators;
using WaterCounter.Services.Interfaces.Adapters;
using WaterCounter.Tests.TestHelpers;
using WaterCounter.ViewModels;

namespace WaterCounter.Tests.ViewModels
{
    public class MainPageViewModelTests
    {
        private Mock<ICupTypeCommandHandlerDecorator> _cupTypeCommandHandlerDecoratorMock;

        private Mock<IApplicationAdapter> _applicationAdapterMock;

        private Mock<ICupTypesQueryHandlerDecoratorAsync> _cupTypesQueryHandlerDecoratorAsyncMock;

        private Mock<IAddDrankWaterStatusCommandHandlerDecoratorAsync> _addDrankWaterStatusCommandHandlerDecoratorAsyncMock;

        private Mock<IRemoveDrankWaterStatusCommandHandlerDecoratorAsync> _removeDrankWaterStatusCommandHandlerDecoratorAsyncMock;

        private Mock<IDrankWaterDailyStatusQueryHandlerDecorator> _drankWaterDailyStatusQueryHandlerDecorator;

        private Mock<IAlertDialogAdapter> _alertDialogAdapterMock;

        private Mock<IDatabaseCreator> _databaseCreator;

        private MainPageViewModel _serviceUnderTests;

        private ResultWithModel<DrankWaterDailyStatusModelCollection> ExpectedReportsResultWithModel => new ResultWithModel<DrankWaterDailyStatusModelCollection>(new DrankWaterDailyStatusModelCollection(
                new List<DrankWaterStatusReportModel>
                {
                    new DrankWaterStatusReportModel(1, new CupTypeModel(1, "Item1", 1)),
                    new DrankWaterStatusReportModel(2, new CupTypeModel(2, "Item2", 2)),
                    new DrankWaterStatusReportModel(3, new CupTypeModel(3, "Item3", 3))
                }.AsEnumerable(),
                DateTime.Now,
                3050,
                new LiterModel(3, 50)));

        private ResultWithModel<IEnumerable<CupTypeModel>> ExpectedCupsResultWithModel => new ResultWithModel<IEnumerable<CupTypeModel>>(new List<CupTypeModel>
                {
                    new CupTypeModel(1, "Item1", 1),
                    new CupTypeModel(2, "Item2", 2),
                    new CupTypeModel(3, "Item3", 3),
                }.AsEnumerable());

        [SetUp]
        public void SetUp()
        {
            _cupTypeCommandHandlerDecoratorMock = new Mock<ICupTypeCommandHandlerDecorator>();
            _applicationAdapterMock = new Mock<IApplicationAdapter>();
            _cupTypesQueryHandlerDecoratorAsyncMock = new Mock<ICupTypesQueryHandlerDecoratorAsync>();
            _addDrankWaterStatusCommandHandlerDecoratorAsyncMock = new Mock<IAddDrankWaterStatusCommandHandlerDecoratorAsync>();
            _removeDrankWaterStatusCommandHandlerDecoratorAsyncMock = new Mock<IRemoveDrankWaterStatusCommandHandlerDecoratorAsync>();
            _drankWaterDailyStatusQueryHandlerDecorator = new Mock<IDrankWaterDailyStatusQueryHandlerDecorator>();
            _alertDialogAdapterMock = new Mock<IAlertDialogAdapter>();
            _databaseCreator = new Mock<IDatabaseCreator>();

            _applicationAdapterMock.SetupGet(mock => mock.Dispatcher).Returns(new DispatcherTest());
            _alertDialogAdapterMock.Setup(mock => mock.DisplayActionSheetAsync(It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>()))
                .Returns(Task.FromResult("Tak"));

            _serviceUnderTests = new MainPageViewModel(_cupTypeCommandHandlerDecoratorMock.Object,
                _applicationAdapterMock.Object,
                _cupTypesQueryHandlerDecoratorAsyncMock.Object,
                _addDrankWaterStatusCommandHandlerDecoratorAsyncMock.Object,
                _drankWaterDailyStatusQueryHandlerDecorator.Object,
                _removeDrankWaterStatusCommandHandlerDecoratorAsyncMock.Object,
                _alertDialogAdapterMock.Object,
                _databaseCreator.Object);
        }

        [Test]
        public async Task OnApearingCommand_CheckIfCupTypesCollectionWillFillWithSuccess()
        {
            _cupTypeCommandHandlerDecoratorMock.Setup(mock => mock.ExecuteAsync(It.IsAny<AddCupTypeCommand>()))
                .Returns(Task.FromResult(Result.Ok()));

            _cupTypesQueryHandlerDecoratorAsyncMock.Setup(mock => mock.HandleAsync())
                .Returns(Task.FromResult(ExpectedCupsResultWithModel));

            _drankWaterDailyStatusQueryHandlerDecorator.Setup(mock => mock.HandleAsync(It.IsAny<DrankWaterDailyStatusQuery>()))
                .Returns(Task.FromResult(ExpectedReportsResultWithModel));

            await _serviceUnderTests.OnApearingCommand.ExecuteAsync();

            _cupTypesQueryHandlerDecoratorAsyncMock.Verify(mock => mock.HandleAsync(), Times.Once);
            _drankWaterDailyStatusQueryHandlerDecorator.Verify(mock => mock.HandleAsync(It.IsAny<DrankWaterDailyStatusQuery>()), Times.Once);

            _serviceUnderTests.ErrorMessage.Should().BeNullOrEmpty();
            _serviceUnderTests.IsInvalid.Should().BeFalse();
            _serviceUnderTests.CupTypeModels.ToList().Should().BeEquivalentTo(ExpectedCupsResultWithModel.Model);
        }

        [Test]
        public async Task OnApearingCommand_When_Any_Error_Occours_While_Adding_CupType_To_Database_Then_Dont_Fill_CupTypeCollection()
        {
            _applicationAdapterMock.SetupGet(mock => mock.IsDebugMode).Returns(true);

            _cupTypeCommandHandlerDecoratorMock.Setup(mock => mock.ExecuteAsync(It.IsAny<AddCupTypeCommand>()))
                .Returns(Task.FromResult(Result.Fail("Test message")));

            _cupTypesQueryHandlerDecoratorAsyncMock.Setup(mock => mock.HandleAsync())
                .Returns(Task.FromResult(ExpectedCupsResultWithModel));

            _drankWaterDailyStatusQueryHandlerDecorator.Setup(mock => mock.HandleAsync(It.IsAny<DrankWaterDailyStatusQuery>()))
                .Returns(Task.FromResult(ExpectedReportsResultWithModel));

            await _serviceUnderTests.OnApearingCommand.ExecuteAsync();

            _drankWaterDailyStatusQueryHandlerDecorator.Verify(mock => mock.HandleAsync(It.IsAny<DrankWaterDailyStatusQuery>()), Times.Never);
            _cupTypesQueryHandlerDecoratorAsyncMock.Verify(mock => mock.HandleAsync(), Times.Never);

            _serviceUnderTests.ErrorMessage.Should().NotBeEmpty();
            _serviceUnderTests.IsInvalid.Should().BeTrue();
            _serviceUnderTests.ErrorMessage.Should().Be("Test message");
            _serviceUnderTests.CupTypeModels.ToList().Should().BeEmpty();
            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.AddedCupsResult.Should().Be($"Ilość wypitych płynów z dnia 0001-01-01: 0 litrów i 0 militrów");
        }

        [Test]
        public async Task OnAddNewCupCommand_WhenUserChoseCupType_ThenCollectionWillBeIncreasebByThisItem()
        {
            SetTodayReportDate();

            var exampleResult = new DrankWaterStatusReportModel(1, new CupTypeModel(1, "750 ml", 750));

            _addDrankWaterStatusCommandHandlerDecoratorAsyncMock.Setup(mock => mock.ExecuteAsync(It.IsAny<AddDrankWaterStatusCommand>()))
                .Returns(Task.FromResult(Result.Ok<DrankWaterStatusReportModel>(exampleResult)));

            _drankWaterDailyStatusQueryHandlerDecorator.Setup(mock => mock.HandleAsync(It.IsAny<DrankWaterDailyStatusQuery>()))
                .Returns(Task.FromResult(ExpectedReportsResultWithModel));

            await _serviceUnderTests.OnAddNewCupCommand.ExecuteAsync(new CupTypeModel(1, "Item1", 2050));
            await _serviceUnderTests.OnAddNewCupCommand.ExecuteAsync(new CupTypeModel(2, "Item2", 500));
            await _serviceUnderTests.OnAddNewCupCommand.ExecuteAsync(new CupTypeModel(3, "Item3", 500));

            _serviceUnderTests.ErrorMessage.Should().BeNullOrEmpty();
            _serviceUnderTests.IsInvalid.Should().BeFalse();
            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.DrankWaterStatusReportModels.Count().Should().Be(3);

            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.AddedCupsResult.Should().Be($"Ilość płynów wypitych dzisiaj: 2 litrów i 250 militrów");
        }

        [Test]
        public async Task OnAddNewCupCommand_WhenWhileAddingNewCupOccoursError_ThenCollectionWillNotBeIncreasebByThisItem()
        {
            SetTodayReportDate();

            _addDrankWaterStatusCommandHandlerDecoratorAsyncMock.Setup(mock => mock.ExecuteAsync(It.IsAny<AddDrankWaterStatusCommand>()))
                .Returns(Task.FromResult(Result.FailWithModel<DrankWaterStatusReportModel>("Test message")));

            _drankWaterDailyStatusQueryHandlerDecorator.Setup(mock => mock.HandleAsync(It.IsAny<DrankWaterDailyStatusQuery>()))
                .Returns(Task.FromResult(ExpectedReportsResultWithModel));

            await _serviceUnderTests.OnAddNewCupCommand.ExecuteAsync(new CupTypeModel(1, "Item1", 1));

            _serviceUnderTests.ErrorMessage.Should().NotBeNullOrEmpty();
            _serviceUnderTests.IsInvalid.Should().BeTrue();
            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.DrankWaterStatusReportModels.ToList().Should().BeEmpty();
            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.AddedCupsResult.Should().Be($"Ilość płynów wypitych dzisiaj: 0 litrów i 0 militrów");
        }

        [Test]
        public async Task OnAddNewCupCommand_WhenWhileFetchinDataOccoursError_ThenCollectionWillNotBeIncreasebByThisItem()
        {
            SetTodayReportDate();

            var expectedResultWithModel = new ResultWithModel<DrankWaterDailyStatusModelCollection>(false);

            _addDrankWaterStatusCommandHandlerDecoratorAsyncMock.Setup(mock => mock.ExecuteAsync(It.IsAny<AddDrankWaterStatusCommand>()))
                .Returns(Task.FromResult(Result.FailWithModel<DrankWaterStatusReportModel>("Test message")));

            _drankWaterDailyStatusQueryHandlerDecorator.Setup(mock => mock.HandleAsync(It.IsAny<DrankWaterDailyStatusQuery>()))
                .Returns(Task.FromResult(expectedResultWithModel));

            await _serviceUnderTests.OnAddNewCupCommand.ExecuteAsync(new CupTypeModel(1, "Item1", 1));

            _serviceUnderTests.ErrorMessage.Should().NotBeNullOrEmpty();
            _serviceUnderTests.IsInvalid.Should().BeTrue();
            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.DrankWaterStatusReportModels.ToList().Should().BeEmpty();
            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.AddedCupsResult.Should().Be($"Ilość płynów wypitych dzisiaj: 0 litrów i 0 militrów");
        }

        [Test]
        public async Task OnAddNewCupCommand_WhenCommandParametersIsNull_ThenSkipAddingItemToCollection()
        {
            SetTodayReportDate();

            var expectedResultWithModel = new ResultWithModel<DrankWaterDailyStatusModelCollection>(false);

            _addDrankWaterStatusCommandHandlerDecoratorAsyncMock.Setup(mock => mock.ExecuteAsync(It.IsAny<AddDrankWaterStatusCommand>()))
                .Returns(Task.FromResult(Result.FailWithModel<DrankWaterStatusReportModel>("Test message")));

            _drankWaterDailyStatusQueryHandlerDecorator.Setup(mock => mock.HandleAsync(It.IsAny<DrankWaterDailyStatusQuery>()))
                .Returns(Task.FromResult(expectedResultWithModel));

            await _serviceUnderTests.OnAddNewCupCommand.ExecuteAsync(null);

            _addDrankWaterStatusCommandHandlerDecoratorAsyncMock.Verify(mock => mock.ExecuteAsync(It.IsAny<AddDrankWaterStatusCommand>()),
                Times.Never);

            _serviceUnderTests.ErrorMessage.Should().NotBeEmpty();
            _serviceUnderTests.IsInvalid.Should().BeTrue();
            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.DrankWaterStatusReportModels.ToList().Should().BeEmpty();
            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.AddedCupsResult.Should().Be($"Ilość płynów wypitych dzisiaj: 0 litrów i 0 militrów");
        }

        private void SetTodayReportDate()
        {
            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.InitializeData(DateTime.Now);
        }

        [Test]
        public async Task OnDeleteCommand_WhenCommandHandlerReturnInvalidResponse_ThenShowErrorAndDoesntRefreshData()
        {
            var expectedResultWithModel = new ResultWithModel<DrankWaterDailyStatusModelCollection>(false);

            _removeDrankWaterStatusCommandHandlerDecoratorAsyncMock.Setup(mock => mock.ExecuteAsync(It.IsAny<RemoveDranlWaterStatusItemCommand>()))
                .Returns(Task.FromResult(Result.Fail("Test message")));

            _drankWaterDailyStatusQueryHandlerDecorator.Setup(mock => mock.HandleAsync(It.IsAny<DrankWaterDailyStatusQuery>()))
                .Returns(Task.FromResult(expectedResultWithModel));

            await _serviceUnderTests.OnDeleteCommand.ExecuteAsync(1);

            _removeDrankWaterStatusCommandHandlerDecoratorAsyncMock.Verify(mock => mock.ExecuteAsync(It.IsAny<RemoveDranlWaterStatusItemCommand>()),
                Times.Once);

            _serviceUnderTests.ErrorMessage.Should().NotBeEmpty();
            _serviceUnderTests.IsInvalid.Should().BeTrue();
        }

        [Test]
        public async Task OnDeleteCommand_WhenCommandHandlerReturnValidResponse_ThenRefreshData()
        {
            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.DrankWaterStatusReportModels.AddRange(new List<DrankWaterStatusReportModel>
            {
                new DrankWaterStatusReportModel(1, new CupTypeModel(1, "250 ml", 250)),
                new DrankWaterStatusReportModel(2, new CupTypeModel(2, "500 ml", 500))
            });

            _serviceUnderTests.DrankWaterStatusReportCollectionViewModel.LiterModel = new LiterModel(5, 250);

            _removeDrankWaterStatusCommandHandlerDecoratorAsyncMock.Setup(mock => mock.ExecuteAsync(It.IsAny<RemoveDranlWaterStatusItemCommand>()))
                .Returns(Task.FromResult(Result.Ok()));

            _drankWaterDailyStatusQueryHandlerDecorator.Setup(mock => mock.HandleAsync(It.IsAny<DrankWaterDailyStatusQuery>()))
                .Returns(Task.FromResult(ExpectedReportsResultWithModel));

            await _serviceUnderTests.OnDeleteCommand.ExecuteAsync(1);

            _removeDrankWaterStatusCommandHandlerDecoratorAsyncMock.Verify(mock => mock.ExecuteAsync(It.IsAny<RemoveDranlWaterStatusItemCommand>()),
                Times.Once);

            _serviceUnderTests.ErrorMessage.Should().BeNullOrEmpty();
            _serviceUnderTests.IsInvalid.Should().BeFalse();
        }

        [Test]
        public async Task OnDeleteCommand_WhenUserCancelProcess_ThenDoNotDoNoAction()
        {
            _alertDialogAdapterMock.Setup(mock => mock.DisplayActionSheetAsync(It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>()))
                .Returns(Task.FromResult("Nie"));

            _removeDrankWaterStatusCommandHandlerDecoratorAsyncMock.Setup(mock => mock.ExecuteAsync(It.IsAny<RemoveDranlWaterStatusItemCommand>()))
                .Returns(Task.FromResult(Result.Ok()));

            _drankWaterDailyStatusQueryHandlerDecorator.Setup(mock => mock.HandleAsync(It.IsAny<DrankWaterDailyStatusQuery>()))
                .Returns(Task.FromResult(ExpectedReportsResultWithModel));

            await _serviceUnderTests.OnDeleteCommand.ExecuteAsync(1);

            Thread.Sleep(200);

            _removeDrankWaterStatusCommandHandlerDecoratorAsyncMock.Verify(mock => mock.ExecuteAsync(It.IsAny<RemoveDranlWaterStatusItemCommand>()),
                Times.Never);

            _serviceUnderTests.ErrorMessage.Should().BeNullOrEmpty();
            _serviceUnderTests.IsInvalid.Should().BeFalse();
        }

    }
}