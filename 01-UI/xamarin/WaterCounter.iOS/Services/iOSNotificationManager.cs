﻿using System;
using UserNotifications;
using WaterCounter.Services.Interfaces.MulitPlatformMangers;
using Xamarin.Forms;

[assembly: Dependency(typeof(WaterCounter.iOS.iOSNotificationManager))]
namespace WaterCounter.iOS
{
    public class iOSNotificationManager : INotificationManager
    {
        public int NotificationId = 1;

        bool hasNotificationsPermission;

        bool localNotificationStarted = false;

        int messageId = 0;

        public void Initialize()
        {
            // request the permission to use local notifications
            UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert, (approved, err) =>
            {
                hasNotificationsPermission = approved;
            });
        }

        public void CancelAndStart()
        {
            Cancel();
            Start();
        }

        public void Start()
        {
            if (!hasNotificationsPermission || localNotificationStarted)
            {
                return;
            }

            var content = new UNMutableNotificationContent()
            {
                Title = "Wypij wodę",
                Subtitle = "",
                Body = "Nie piłeś ostatnio wody, napij sie!",
                Badge = 1
            };

            int threeHoursInMinutes = 3 * (60 * 60);
            UNNotificationTrigger trigger = UNTimeIntervalNotificationTrigger.CreateTrigger(threeHoursInMinutes, true);

            messageId++;

            var request = UNNotificationRequest.FromIdentifier(messageId.ToString(), content, trigger);
            UNUserNotificationCenter.Current.AddNotificationRequest(request, (err) =>
            {
                if (err != null)
                {
                    throw new Exception($"Failed to schedule notification: {err}");
                }
            });

            localNotificationStarted = true;
        }

        public void Cancel()
        {
            if (!hasNotificationsPermission)
            {
                return;
            }

            UNUserNotificationCenter.Current.RemoveAllPendingNotificationRequests();

            localNotificationStarted = false;
        }
    }
}