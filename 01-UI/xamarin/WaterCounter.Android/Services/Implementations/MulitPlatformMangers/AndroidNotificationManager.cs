﻿using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using AndroidX.Core.App;
using WaterCounter.Services.Interfaces.MulitPlatformMangers;
using Xamarin.Forms;
using AndroidApp = Android.App.Application;

//to implement later!!!
[assembly: Dependency(typeof(WaterCounter.Droid.Services.Implementations.MulitPlatformMangers.AndroidNotificationManager))]
namespace WaterCounter.Droid.Services.Implementations.MulitPlatformMangers
{
    public class AndroidNotificationManager : INotificationManager
    {
        public void Cancel()
        {
            
        }

        public void CancelAndStart()
        {
            
        }

        public void Initialize()
        {
            
        }

        public void Start()
        {
            
        }
    }
}