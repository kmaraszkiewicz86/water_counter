﻿using System;
using WaterCounter.Services.Interfaces.Mediators;
using WaterCounter.ViewModels;

namespace WaterCounter.Services.Implementations.Mediators
{
    public class OnAppResumeCheckerMediator : IOnAppResumeCheckerMediator
    {
        private readonly MainPageViewModel _mainPageViewModel;

        public OnAppResumeCheckerMediator(MainPageViewModel mainPageViewModel)
        {
            _mainPageViewModel = mainPageViewModel;
        }

        public void OnResume()
        {
            _mainPageViewModel.OnApearingCommand.Execute(true);
        }
    }
}
