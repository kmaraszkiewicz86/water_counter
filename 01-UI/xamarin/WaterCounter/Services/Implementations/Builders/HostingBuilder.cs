﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WaterCounter.Extension;

namespace WaterCounter.Services.Implementations.Builders
{
    public class HostingBuilder
    {
        public static IServiceProvider ServiceProvider { get; private set; }

        private IHostBuilder _hostingBuilder;

        private string _appDataDirectory;

        public HostingBuilder InitializeRequiredServices(string appDataDirectory)
        {
            _appDataDirectory = appDataDirectory;

            _hostingBuilder = new HostBuilder()
                .ConfigureHostConfiguration(ConfigureHostConfiguration)
                .ConfigureServices(ConfigureServices);

            return this;
        }

        public void Build()
        {
            ServiceProvider = _hostingBuilder.Build().Services;
        }

        private void ConfigureHostConfiguration(IConfigurationBuilder configurationBuilder)
        {
            configurationBuilder.AddCommandLine(
                new[] { $"ContentRoot={_appDataDirectory}" });
        }

        private void ConfigureServices(IServiceCollection serviceCollection)
        {
            serviceCollection.GetDbServices(_appDataDirectory)
                .GetFactories()
                .GetMediators()
                .GetAdapters()
                .GetBridges()
                .GetViewModels()
                .GetPages();
        }
    }
}