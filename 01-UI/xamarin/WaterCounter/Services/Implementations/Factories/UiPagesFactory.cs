﻿using WaterCounter.Pages;
using WaterCounter.Services.Interfaces.Factories;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static WaterCounter.Services.Implementations.Builders.HostingBuilder;

namespace WaterCounter.Services.Implementations.Factories
{
    public class UiPagesFactory : IUiPagesFactory
    {
        public MainPage MainPage => ServiceProvider.GetService<MainPage>();

        public SettingsPage SettingsPage => ServiceProvider.GetService<SettingsPage>();

        public CalendarOfHistoryPage CalendarOfHistoryPage => ServiceProvider.GetService<CalendarOfHistoryPage>();
    }
}