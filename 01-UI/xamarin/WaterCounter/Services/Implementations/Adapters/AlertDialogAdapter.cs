﻿using System.Threading.Tasks;
using WaterCounter.Services.Interfaces.Adapters;

namespace WaterCounter.Services.Implementations.Adapters
{
    public class AlertDialogAdapter : IAlertDialogAdapter
    {
        private IApplicationAdapter _applicationAdapter;

        public AlertDialogAdapter(IApplicationAdapter applicationAdapter)
        {
            _applicationAdapter = applicationAdapter;
        }

        public async Task DisplayAlertAsync(string title, string message)
        {
            await _applicationAdapter.MainPage.DisplayAlert(title, message, "OK");
        }

        public async Task<bool> DisplayAlertWithChoiceAsync(string title, string message, string acceptText, string cancelText)
        {
            return await _applicationAdapter.MainPage.DisplayAlert(title, message, acceptText, cancelText);
        }
    }
}