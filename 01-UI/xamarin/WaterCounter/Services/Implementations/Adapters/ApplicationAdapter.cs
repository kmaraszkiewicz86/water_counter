﻿using WaterCounter.Services.Interfaces.Adapters;
using Xamarin.Forms;

namespace WaterCounter.Services.Implementations.Adapters
{
    public class ApplicationAdapter : IApplicationAdapter
    {
        public Page MainPage => App.Current.MainPage;

        public INavigation Navigation => MainPage.Navigation;

        public IDispatcher Dispatcher => MainPage.Dispatcher;

        public bool IsDebugMode
        {
            get
            {
#if RELEASE
                return false;
#else
                return true;
#endif
            }
        }
    }
}