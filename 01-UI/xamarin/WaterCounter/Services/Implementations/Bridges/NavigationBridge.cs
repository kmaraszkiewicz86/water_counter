﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WaterCounter.Enums;
using WaterCounter.Pages;
using WaterCounter.Services.Interfaces.Adapters;
using WaterCounter.Services.Interfaces.Bridges;
using WaterCounter.Services.Interfaces.Factories;
using Xamarin.Forms;

namespace WaterCounter.Services.Implementations.Bridges
{
    public sealed class NavigationBridge : INavigationBridge
    {
        private readonly IApplicationAdapter _applicationAdapter;

        private readonly IUiPagesFactory _uiPagesFactory;

        public NavigationBridge(IApplicationAdapter applicationAdapter, IUiPagesFactory uiPagesFactory)
        {
            _applicationAdapter = applicationAdapter;
            _uiPagesFactory = uiPagesFactory;
        }

        public async Task NavigationToPageByType(NavigationPageType navigationPageType)
        {
            switch (navigationPageType)
            {
                case NavigationPageType.MainPage:

                    await _applicationAdapter.Navigation.PopToRootAsync();
                    break;

                case NavigationPageType.SettingsPage:

                    if (IsCurrentPageOnTheNavigationStack<SettingsPage>())
                        return;

                    await _applicationAdapter.Navigation.PushAsync(_uiPagesFactory.SettingsPage);
                    break;

                case NavigationPageType.CalendarOfHistory:

                    if (IsCurrentPageOnTheNavigationStack<CalendarOfHistoryPage>())
                        return;

                    await _applicationAdapter.Navigation.PushAsync(_uiPagesFactory.CalendarOfHistoryPage);
                    break;
            }
        }

        private bool IsCurrentPageOnTheNavigationStack<TPage>()
            where TPage : Page
        {
            foreach(Page page in _applicationAdapter.Navigation.NavigationStack)
            {
                if (page is TPage)
                    return true;
            }

            return false;
        }
    }
}