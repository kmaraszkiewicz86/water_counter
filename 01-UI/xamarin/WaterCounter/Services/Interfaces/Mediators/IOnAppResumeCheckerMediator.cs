﻿using System;
namespace WaterCounter.Services.Interfaces.Mediators
{
    public interface IOnAppResumeCheckerMediator
    {
        void OnResume();
    }
}