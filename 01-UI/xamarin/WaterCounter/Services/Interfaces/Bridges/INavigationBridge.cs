﻿using System.Threading.Tasks;
using WaterCounter.Enums;

namespace WaterCounter.Services.Interfaces.Bridges
{
    public interface INavigationBridge
    {
        Task NavigationToPageByType(NavigationPageType navigationPageType);
    }
}