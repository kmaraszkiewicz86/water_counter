﻿using System;

namespace WaterCounter.Services.Interfaces.MulitPlatformMangers
{
    public interface INotificationManager
    {
        void Initialize();

        void Start();

        void Cancel();
        void CancelAndStart();
    }
}