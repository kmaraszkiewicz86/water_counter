﻿using System.Threading.Tasks;

namespace WaterCounter.Services.Interfaces.Adapters
{
    public interface IAlertDialogAdapter
    {
        Task DisplayAlertAsync(string title, string message);

        Task<bool> DisplayAlertWithChoiceAsync(string title, string message, string acceptText, string cancelText);
    }
}