﻿using Xamarin.Forms;

namespace WaterCounter.Services.Interfaces.Adapters
{
    public interface IApplicationAdapter
    {
        Page MainPage { get; }

        INavigation Navigation { get; }

        IDispatcher Dispatcher { get; }

        bool IsDebugMode { get; }
    }
}