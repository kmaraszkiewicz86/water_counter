﻿using System;
using System.Collections.Generic;
using System.Text;
using WaterCounter.Pages;

namespace WaterCounter.Services.Interfaces.Factories
{
    public interface IUiPagesFactory
    {
        MainPage MainPage { get; }
        SettingsPage SettingsPage { get; }
        CalendarOfHistoryPage CalendarOfHistoryPage { get; }
    }
}