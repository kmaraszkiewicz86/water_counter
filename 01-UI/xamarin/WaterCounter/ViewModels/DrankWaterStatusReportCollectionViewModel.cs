﻿using System;
using System.Linq;
using WaterCounter.Models.Models;
using Xamarin.CommunityToolkit.ObjectModel;

namespace WaterCounter.ViewModels
{
    public class DrankWaterStatusReportCollectionViewModel
    {
        public MainPageViewModel ParentViewModel { get; set; }

        public int SpanLengthForDrankWaterCups { get; } = 6;

        public ObservableRangeCollection<DrankWaterStatusReportModel> DrankWaterStatusReportModels { get; }
            = new ObservableRangeCollection<DrankWaterStatusReportModel>();

        public int DrankWaterStatusReportModelsHeight
        {
            get
            {
                int itemsRows = DrankWaterStatusReportModels.Count / SpanLengthForDrankWaterCups;

                return (itemsRows + 1) * 50;
            }
        }

        public bool IsReportDateToday
        {
            get
            {
                var currentDate = DateTime.Now;

                return ReportDay.Day == currentDate.Day
                    && ReportDay.Month == currentDate.Month
                    && ReportDay.Year == currentDate.Year;
            }
        }

        public DateTime ReportDay { get; private set; }

        public bool IsDrunkWaterLiterExceded
        {
            get
            {
                if (LiterModel?.Liters >= 15)
                {
                    return true;
                }

                return false;
            }
        }

        public LiterModel LiterModel
        {
            get => _literModel;
            set
            {
                _literModel = value;
                ParentViewModel?.OnPropertyChanged();
                ParentViewModel?.OnPropertyChanged(nameof(AddedCupsResult));
            }
        }

        public string AddedCupsResult
        {
            get
            {
                var preMessage = $"Ilość płynów wypitych dzisiaj:";

                if (!IsReportDateToday)
                {
                    var reportDateInString = ReportDay.ToString("yyyy-MM-dd");
                    preMessage = $"Ilość wypitych płynów z dnia {reportDateInString}:";
                }


                return $"{preMessage} {LiterModel?.Liters ?? 0} litrów i {LiterModel?.Mililiters ?? 0} militrów";
            }
        }

        private LiterModel _literModel;

        public DrankWaterStatusReportCollectionViewModel()
        {
            LiterModel = new LiterModel(0, 0);
        }

        public void RemoveItemFromCollection(int id)
        {
            DrankWaterStatusReportModel model = DrankWaterStatusReportModels.FirstOrDefault(d => d.Id == id);

            if (model == null)
                return;

            DrankWaterStatusReportModels.Remove(model);
            LiterModel.Substract(model.Value);

            RefreshUI();
        }

        public void AddDrankWaterDailyStatus(DrankWaterStatusReportModel model)
        {
            DrankWaterStatusReportModels.Add(model);
            LiterModel.Add(model.Value);

            RefreshUI();
        }

        public void InitializeData(DateTime currentDate)
        {
            ReportDay = currentDate;
            LiterModel = new LiterModel(0, 0);

            DrankWaterStatusReportModels.Clear();

            RefreshUI();
        }

        public void AddItemsToCollection(ResultWithModel<DrankWaterDailyStatusModelCollection> model)
        {
            ReportDay = model.Model.CurrentDate;
            LiterModel = model.Model.LiterModelSummary;

            DrankWaterStatusReportModels.AddRange(model.Model.DrankWaterStatusReportModels);

            RefreshUI();
        }

        private void RefreshUI()
        {
            ParentViewModel?.OnPropertyChanged(nameof(DrankWaterStatusReportCollectionViewModel));
        }
    }
}