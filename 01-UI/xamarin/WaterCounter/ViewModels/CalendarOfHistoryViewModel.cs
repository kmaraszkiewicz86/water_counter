﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.Decorators;
using WaterCounter.Services.Interfaces.Bridges;
using Xamarin.CommunityToolkit.ObjectModel;

namespace WaterCounter.ViewModels
{
    public class CalendarOfHistoryViewModel : NavigationViewModelBase
    {
        public ObservableRangeCollection<DrankWaterReportByDayModel> DrankWaterReportByDayModels { get; } = new ObservableRangeCollection<DrankWaterReportByDayModel>();

        public AsyncCommand OnApearingCommand { get; }

        private readonly IDrankWaterReportByDayModelQueryHandlerDecoratorAsync _drankWaterReportByDayModelQueryHandlerDecoratorAsync;

        public CalendarOfHistoryViewModel(IDrankWaterReportByDayModelQueryHandlerDecoratorAsync drankWaterReportByDayModelQueryHandlerDecoratorAsync,
            INavigationBridge navigationBridge) : base(navigationBridge)
        {
            OnApearingCommand = new AsyncCommand(OnAppearingCommandAction, allowsMultipleExecutions: false);

            _drankWaterReportByDayModelQueryHandlerDecoratorAsync = drankWaterReportByDayModelQueryHandlerDecoratorAsync;
        }

        private async Task OnAppearingCommandAction()
        {
            ResultWithModel<IEnumerable<DrankWaterReportByDayModel>> result = await _drankWaterReportByDayModelQueryHandlerDecoratorAsync.HandleAsync();

            if (result.IsValid && !result.IsModelNull)
            {
                DrankWaterReportByDayModels.Clear();
                DrankWaterReportByDayModels.AddRange(result.Model);
            }
        }
    }
}