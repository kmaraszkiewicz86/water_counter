﻿using System;
using System.Threading.Tasks;
using WaterCounter.Services.Interfaces.Bridges;

namespace WaterCounter.ViewModels
{
    public abstract class LoadingViewModelBase : NavigationViewModelBase
    {
        public bool IsCommandsActive => !DataIsLoading;

        public bool DataIsLoading
        {
            get => _dataIsLoading;
            set
            {
                _dataIsLoading = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsCommandsActive));
            }
        }

        private bool _dataIsLoading;

        protected LoadingViewModelBase(INavigationBridge navigationBridge) : base(navigationBridge)
        {
        }

        protected virtual async Task OnDataLoadingAsync(Func<Task> onActionAsync)
        {
            try
            {
                DataIsLoading = true;

                await onActionAsync();
            }
            finally
            {
                DataIsLoading = false;
            }
        }
    }
}