﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaterCounter.Models.Enums;
using WaterCounter.Models.Extensions;
using WaterCounter.Models.Models;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Helpers;
using WaterCounter.Service.Services.Implementations.Commands;
using WaterCounter.Service.Services.Implementations.Queries;
using WaterCounter.Service.Services.Interfaces.Decorators;
using WaterCounter.Services.Interfaces.Adapters;
using WaterCounter.Services.Interfaces.Bridges;
using WaterCounter.Services.Interfaces.MulitPlatformMangers;
using Xamarin.CommunityToolkit.ObjectModel;
using Xamarin.Forms;

namespace WaterCounter.ViewModels
{
    public sealed class MainPageViewModel : LoadingViewModelBase
    {
        public IAsyncCommand<bool> OnApearingCommand { get; }

        public IAsyncCommand<CupTypeModel> OnAddNewCupCommand { get; }

        public IAsyncCommand<int> OnDeleteCommand { get; }

        public IAsyncCommand<string> OnLoadPreviousDrankReportCommand { get; }

        public ObservableRangeCollection<CupTypeModel> CupTypeModels { get; } = new ObservableRangeCollection<CupTypeModel>();

        public ObservableRangeCollection<int> DaysNavigatation { get; } = new ObservableRangeCollection<int>();

        public DrankWaterStatusReportCollectionViewModel DrankWaterStatusReportCollectionViewModel { get; }
            = new DrankWaterStatusReportCollectionViewModel();

        public bool IsInvalid
        {
            get => _isInvalid;
            set
            {
                _isInvalid = value;
                OnPropertyChanged();
            }
        }

        public string ErrorMessage
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_errorMessage))
                    return string.Empty;

                var isdebugMode = _applicationAdapter?.IsDebugMode ?? false;

                return !isdebugMode
                    ? "Wystąpił nieoczekiwany błąd, proszę spróbować poźniej"
                    : _errorMessage;
            }
            set
            {
                _errorMessage = value;
                OnPropertyChanged();
            }
        }

        private bool IsMinimalLitersDrunkWaterExceted => DrankWaterStatusReportCollectionViewModel.LiterModel.Liters >= 3;

        private bool _isInvalid;

        private string _errorMessage;

        private bool _isStarted = false;

        private DateTime _currentDateForReport;

        private readonly ICupTypeCommandHandlerDecorator _cupTypeCommandHandlerDecorator;

        private readonly IApplicationAdapter _applicationAdapter;

        private readonly ICupTypesQueryHandlerDecoratorAsync _cupTypesQueryHandlerDecoratorAsync;

        private readonly IAddDrankWaterStatusCommandHandlerDecoratorAsync _addDrankWaterStatusCommandHandlerDecoratorAsync;

        private readonly IRemoveDrankWaterStatusCommandHandlerDecoratorAsync _removeDrankWaterStatusCommandHandlerDecoratorAsync;

        private readonly IDrankWaterDailyStatusQueryHandlerDecorator _drankWaterDailyStatusQueryHandlerDecorator;

        private readonly IAlertDialogAdapter _alertDialogAdapter;

        private readonly IDatabaseCreator _databaseCreator;

        private INotificationManager _notificationManager;

        public MainPageViewModel(ICupTypeCommandHandlerDecorator cupTypeCommandHandlerDecorator,
            IApplicationAdapter applicationAdapter,
            ICupTypesQueryHandlerDecoratorAsync cupTypesQueryHandlerDecoratorAsync,
            IAddDrankWaterStatusCommandHandlerDecoratorAsync addDrankWaterStatusCommandHandlerDecoratorAsync,
            IDrankWaterDailyStatusQueryHandlerDecorator drankWaterDailyStatusQueryHandlerDecoratorAsync,
            IRemoveDrankWaterStatusCommandHandlerDecoratorAsync removeDrankWaterStatusCommandHandlerDecoratorAsync,
            IAlertDialogAdapter alertDialogAdapter,
            IDatabaseCreator databaseCreator,
            INavigationBridge navigationBridge)
            : base(navigationBridge)
        {
            OnApearingCommand = new AsyncCommand<bool>(OnApearingCommandActionAsync, canExecute: (obj) => IsCommandsActive, allowsMultipleExecutions: false);
            OnAddNewCupCommand = new AsyncCommand<CupTypeModel>(OnAddNewCupActionAsync, canExecute: (obj) => IsCommandsActive, allowsMultipleExecutions: false);
            OnDeleteCommand = new AsyncCommand<int>(OnDeleteCommandActionAsync, canExecute: (obj) => IsCommandsActive, allowsMultipleExecutions: false);
            OnLoadPreviousDrankReportCommand = new AsyncCommand<string>(OnDrankReportCommandActionAsync, canExecute: (obj) => IsCommandsActive, allowsMultipleExecutions: false);

            _currentDateForReport = DateTime.Now;

            _cupTypeCommandHandlerDecorator = cupTypeCommandHandlerDecorator;
            _applicationAdapter = applicationAdapter;
            _cupTypesQueryHandlerDecoratorAsync = cupTypesQueryHandlerDecoratorAsync;
            _addDrankWaterStatusCommandHandlerDecoratorAsync = addDrankWaterStatusCommandHandlerDecoratorAsync;
            _drankWaterDailyStatusQueryHandlerDecorator = drankWaterDailyStatusQueryHandlerDecoratorAsync;
            _removeDrankWaterStatusCommandHandlerDecoratorAsync = removeDrankWaterStatusCommandHandlerDecoratorAsync;
            _alertDialogAdapter = alertDialogAdapter;
            _databaseCreator = databaseCreator;

            _notificationManager = DependencyService.Get<INotificationManager>();
        }

        protected override Task OnDataLoadingAsync(Func<Task> onActionAsync)
        {
            try
            {
                return base.OnDataLoadingAsync(onActionAsync);

            }
            finally
            {
                DataIsLoading = false;
                RefreshButtonsState();
            }
        }

        public async Task OnApearingCommandActionAsync(bool shouldLoadDataForCurrentDate)
        {
            if (!_isStarted && !IsMinimalLitersDrunkWaterExceted)
            {
                _notificationManager.CancelAndStart();
                _isStarted = true;
            }

            await OnDataLoadingAsync(async () =>
            {
                await LoadingRequiredDataAsync(shouldLoadDataForCurrentDate);
            });
        }

        private async Task LoadingRequiredDataAsync(bool shouldLoadDataForCurrentDate)
        {
            DrankWaterStatusReportCollectionViewModel.ParentViewModel = this;

            try
            {
                await _databaseCreator.InitializeDatabaseAsync();

                Result result = await _cupTypeCommandHandlerDecorator.ExecuteAsync(new AddCupTypeCommand(new List<CupTypeModel>
                {
                    new CupTypeModel("100 ml", 100),
                    new CupTypeModel("250 ml", 250),
                    new CupTypeModel("500 ml", 500)
                }));

                SetResultMessage(result);

                if (!IsInvalid)
                {
                    ResultWithModel<IEnumerable<CupTypeModel>> cupTypeModelsResult = await _cupTypesQueryHandlerDecoratorAsync.HandleAsync();

                    SetResultMessage(cupTypeModelsResult);

                    if (IsInvalid)
                        return;

                    var items = cupTypeModelsResult.Model.ToList();

                    _applicationAdapter.Dispatcher.BeginInvokeOnMainThread(() =>
                    {
                        CupTypeModels.Clear();
                        CupTypeModels.AddRange(items);
                    });

                    if (shouldLoadDataForCurrentDate)
                    {
                        _currentDateForReport = DateTime.Now;
                    }

                    await LoadDrankWaterDailyStatusAsync(_currentDateForReport);
                }
            }
            catch (Exception err)
            {
                await _alertDialogAdapter.DisplayAlertAsync("Uwaga!", err.Message);
            }
        }

        private void RefreshButtonsState()
        {
            OnAddNewCupCommand?.RaiseCanExecuteChanged();
            OnDeleteCommand?.RaiseCanExecuteChanged();
            OnLoadPreviousDrankReportCommand?.RaiseCanExecuteChanged();
        }

        private async Task OnDrankReportCommandActionAsync(string drankReportStatusByDateCriteriaString)
        {
            await OnDataLoadingAsync(async () =>
            {
                await OnDrankReportDateChangeAsync(drankReportStatusByDateCriteriaString);
            });
        }

        private async Task OnDrankReportDateChangeAsync(string drankReportStatusByDateCriteriaString)
        {
            if (!Enum.TryParse<DrankReportStatusByDateCriteria>(drankReportStatusByDateCriteriaString, out var drankReportStatusByDateCriteria))
            {
                return;
            }

            var nextDateForReport = DateTime.Now;

            switch (drankReportStatusByDateCriteria)
            {
                case DrankReportStatusByDateCriteria.PreviousDate:
                    nextDateForReport = DrankWaterStatusReportCollectionViewModel.ReportDay.AddDays(-1);
                    break;

                case DrankReportStatusByDateCriteria.NextDate:
                    nextDateForReport = DrankWaterStatusReportCollectionViewModel.ReportDay.AddDays(1);
                    break;
            }

            _currentDateForReport = nextDateForReport.ReturnValidDate();

            await LoadDrankWaterDailyStatusAsync(_currentDateForReport);
        }

        private async Task OnDeleteCommandActionAsync(int id)
        {
            await OnDataLoadingAsync(async () =>
            {
                await DeleteItemAsync(id);
                ToggleNotificationBasedOnDrankWater();
            });
        }

        private async Task DeleteItemAsync(int id)
        {
            bool clickedButtonName = await _alertDialogAdapter.DisplayAlertWithChoiceAsync("Uwaga!", "Czy na pewno chcesz usunąć ten element?", "Tak", "Nie");

            if (!clickedButtonName)
                return;

            var request = new RemoveDranlWaterStatusItemCommand(id);
            Result result = await _removeDrankWaterStatusCommandHandlerDecoratorAsync.ExecuteAsync(request);

            SetResultMessage(result);

            if (IsInvalid)
                return;

            _applicationAdapter.Dispatcher.BeginInvokeOnMainThread(() =>
            {
                DrankWaterStatusReportCollectionViewModel.RemoveItemFromCollection(id);
            });
        }

        private async Task OnAddNewCupActionAsync(CupTypeModel cupTypeModel)
        {
            await OnDataLoadingAsync(async () =>
            {
                await AddCupItemAsync(cupTypeModel);
                ToggleNotificationBasedOnDrankWater();
                _notificationManager.CancelAndStart();
            });
        }

        private void ToggleNotificationBasedOnDrankWater()
        {
            if (IsMinimalLitersDrunkWaterExceted)
            {
                _notificationManager.Cancel();
            }
            else
            {
                _notificationManager.Start();
            }
        }
        
        private async Task AddCupItemAsync(CupTypeModel cupTypeModel)
        {
            ErrorMessage = string.Empty;

            if (cupTypeModel == null)
            {
                SetResultMessage(false, "Model could not be empty.");
                return;
            }

            if (DrankWaterStatusReportCollectionViewModel.IsDrunkWaterLiterExceded)
            {
                SetResultMessage(false, "Maksymalny limit dzienny to 20 litrów!");
                return;
            }

            AddDrankWaterStatusCommand addDrankWaterStatusCommand = new AddDrankWaterStatusCommand(cupTypeModel, _currentDateForReport);

            ResultWithModel<DrankWaterStatusReportModel> result =
                await _addDrankWaterStatusCommandHandlerDecoratorAsync.ExecuteAsync(addDrankWaterStatusCommand);

            SetResultMessage(result);

            if (IsInvalid)
                return;

            _applicationAdapter.Dispatcher.BeginInvokeOnMainThread(() =>
            {
                DrankWaterStatusReportCollectionViewModel.AddDrankWaterDailyStatus(result.Model);
            });
        }

        private async Task LoadDrankWaterDailyStatusAsync(DateTime dateTime)
        {
            GenerateDaysNavigationData();

            ResultWithModel<DrankWaterDailyStatusModelCollection> model = await _drankWaterDailyStatusQueryHandlerDecorator.HandleAsync(
                    new DrankWaterDailyStatusQuery(dateTime));

            SetResultMessage(model);

            _applicationAdapter.Dispatcher.BeginInvokeOnMainThread(() =>
            {
                DrankWaterStatusReportCollectionViewModel.InitializeData(dateTime);

                if (IsInvalid || model.IsModelNull)
                {
                    return;
                }

                DrankWaterStatusReportCollectionViewModel.AddItemsToCollection(model);
            });
        }

        private void GenerateDaysNavigationData()
        {
            int daysLength = DaysInMonthHelper.GetDaysOfMonth(DateTime.Now);
            List<int> days = Enumerable.Range(1, daysLength).ToList();

            DaysNavigatation.Clear();
            DaysNavigatation.AddRange(days);
        }

        private void SetResultMessage(Result result)
        {
            if (!_applicationAdapter.Dispatcher.IsInvokeRequired)
            {
                SetResultMessageShared(result.IsValid, result.ErrorMessage);
                return;
            }

            _applicationAdapter.Dispatcher.BeginInvokeOnMainThread(() =>
            {
                SetResultMessageShared(result.IsValid, result.ErrorMessage);
            });
        }

        private void SetResultMessage(bool isValid, string errorMessage)
        {
            if (!_applicationAdapter.Dispatcher.IsInvokeRequired)
            {
                SetResultMessageShared(isValid, errorMessage);
                return;
            }

            _applicationAdapter.Dispatcher.BeginInvokeOnMainThread(() =>
            {
                SetResultMessageShared(isValid, errorMessage);
            });
        }

        private void SetResultMessageShared(bool isValid, string errorMessage)
        {
            IsInvalid = !isValid;
            ErrorMessage = errorMessage;
        }
    }
}