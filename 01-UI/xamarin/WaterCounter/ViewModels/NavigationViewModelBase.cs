﻿using System.Threading.Tasks;
using WaterCounter.Enums;
using WaterCounter.Services.Interfaces.Bridges;
using Xamarin.CommunityToolkit.ObjectModel;

namespace WaterCounter.ViewModels
{
    public abstract class NavigationViewModelBase : ViewModelBase
    {
        public IAsyncCommand OnNavigateToMainPageCommand { get; }

        public IAsyncCommand OnNavigateToSettingsCommand { get; }

        public IAsyncCommand OnNavigateToCalenadarOfHistoryCommand { get; }

        private readonly INavigationBridge _navigationBridge;

        protected NavigationViewModelBase(INavigationBridge navigationBridge)
        {
            OnNavigateToMainPageCommand = new AsyncCommand(async () => await NavigationToPage(NavigationPageType.MainPage), allowsMultipleExecutions: false);
            OnNavigateToSettingsCommand = new AsyncCommand(async () => await NavigationToPage(NavigationPageType.SettingsPage), allowsMultipleExecutions: false);
            OnNavigateToCalenadarOfHistoryCommand = new AsyncCommand(async () => await NavigationToPage(NavigationPageType.CalendarOfHistory), allowsMultipleExecutions: false);

            _navigationBridge = navigationBridge;
        }

        private async Task NavigationToPage(NavigationPageType navigationPageType)
        {
            await _navigationBridge.NavigationToPageByType(navigationPageType);
        }
    }
}