﻿using System;
using System.Reflection;
using Xamarin.Forms;

namespace WaterCounter.Extension
{
    public static class ImageSourceExtension
    {
        public static ImageSource ToImageSource(this string source)
        {
            if (source == null)
            {
                return null;
            }

            var imageSource = ImageSource.FromResource(source, typeof(ImageSourceExtension).GetTypeInfo().Assembly);

            return imageSource;
        }
    }
}
