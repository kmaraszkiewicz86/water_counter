﻿using System.IO;
using Microsoft.Extensions.DependencyInjection;
using WaterCounter.Models.Entities;
using WaterCounter.Pages;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Services.Implementations.CommandHandlers;
using WaterCounter.Service.Services.Implementations.Decorators;
using WaterCounter.Service.Services.Implementations.QueryHandlers;
using WaterCounter.Service.Services.Interfaces.CommandHandlers;
using WaterCounter.Service.Services.Interfaces.Decorators;
using WaterCounter.Service.Services.Interfaces.QueryHandlers;
using WaterCounter.Services.Implementations.Adapters;
using WaterCounter.Services.Implementations.Bridges;
using WaterCounter.Services.Implementations.Factories;
using WaterCounter.Services.Implementations.Mediators;
using WaterCounter.Services.Interfaces.Adapters;
using WaterCounter.Services.Interfaces.Bridges;
using WaterCounter.Services.Interfaces.Factories;
using WaterCounter.Services.Interfaces.Mediators;
using WaterCounter.ViewModels;

namespace WaterCounter.Extension
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection GetDbServices(this IServiceCollection serviceCollection, string appDataDirectory)
        {
            string dbPath = Path.Combine(appDataDirectory, "water_counter.db3");

            DatabaseContext databaseContext = new DatabaseContext(dbPath);

            serviceCollection.AddSingleton<IDatabaseContext>((ser) => databaseContext);
            serviceCollection.AddSingleton<IDatabaseCreator, DatabaseCreator>();

            serviceCollection.AddScoped<IRepository<CupType>, Repository<CupType>>();
            serviceCollection.AddScoped<IRepository<DrankWaterStatus>, Repository<DrankWaterStatus>>();
            serviceCollection.AddScoped<IRepository<DrankWaterStatusWithCupType>, Repository<DrankWaterStatusWithCupType>>();

            serviceCollection.AddScoped<ICupTypeCommandHandlerAsync, CupTypeCommandHandlerAsync>();
            serviceCollection.AddScoped<ICupTypeCommandHandlerDecorator, CupTypeCommandHandlerDecoratorAsync>();

            serviceCollection.AddScoped<IAddDrankWaterStatusCommandWithResultHandlerAsync, AddDrankWaterStatusCommandHandlerAsync>();
            serviceCollection.AddScoped<IAddDrankWaterStatusCommandHandlerDecoratorAsync, AddDrankWaterStatusCommandHandlerDecoratorAsync>();

            serviceCollection.AddScoped<IRemoveDrankWaterStatusCommandHandlerAsync, RemoveDrankWaterStatusCommandHandlerAsync>();
            serviceCollection.AddScoped<IRemoveDrankWaterStatusCommandHandlerDecoratorAsync, RemoveDrankWaterStatusCommandHandlerDecoratorAsync>();

            serviceCollection.AddScoped<ICupTypesQueryHandlerAsync, CupTypesQueryHandlerAsync>();
            serviceCollection.AddScoped<ICupTypesQueryHandlerDecoratorAsync, CupTypesQueryHandlerDecoratorAsync>();

            serviceCollection.AddScoped<IDrankWaterDailyStatusQueryHandler, DrankWaterDailyStatusQueryHandler>();
            serviceCollection.AddScoped<IDrankWaterDailyStatusQueryHandlerDecorator, DrankWaterDailyStatusQueryHandlerDecorator>();

            serviceCollection.AddScoped<IDrankWaterReportByDayModelQueryHandler, DrankWaterReportByDayModelQueryHandler>();
            serviceCollection.AddScoped<IDrankWaterReportByDayModelQueryHandlerDecoratorAsync, DrankWaterReportByDayModelQueryHandlerDecoratorAsync>();

            return serviceCollection;
        }

        public static IServiceCollection GetViewModels(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<MainPageViewModel>();
            serviceCollection.AddScoped<CalendarOfHistoryViewModel>();

            return serviceCollection;
        }

        public static IServiceCollection GetPages(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<MainPage>();
            serviceCollection.AddSingleton<SettingsPage>();
            serviceCollection.AddSingleton<CalendarOfHistoryPage>();

            return serviceCollection;
        }

        public static IServiceCollection GetAdapters(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IAlertDialogAdapter, AlertDialogAdapter>();
            serviceCollection.AddSingleton<IApplicationAdapter, ApplicationAdapter>();

            return serviceCollection;
        }

        public static IServiceCollection GetFactories(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IUiPagesFactory, UiPagesFactory>();

            return serviceCollection;
        }

        public static IServiceCollection GetBridges(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<INavigationBridge, NavigationBridge>();

            return serviceCollection;
        }

        public static IServiceCollection GetMediators(this IServiceCollection serviceCollection)
        {

            serviceCollection.AddSingleton<IOnAppResumeCheckerMediator, OnAppResumeCheckerMediator>();

            return serviceCollection;

        }
    }
}