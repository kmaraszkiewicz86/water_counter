﻿using System;
using WaterCounter.Services.Implementations.Builders;
using WaterCounter.Services.Interfaces.Mediators;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using static WaterCounter.Services.Implementations.Builders.HostingBuilder;

namespace WaterCounter
{
    public partial class App : Application
    {
        private IOnAppResumeCheckerMediator _onResumeCheckerMediator => HostingBuilder.ServiceProvider.GetService<IOnAppResumeCheckerMediator>();

        public App()
        {
            InitializeComponent();

            new HostingBuilder()
                .InitializeRequiredServices(FileSystem.AppDataDirectory)
                .Build();

            MainPage = new NavigationPage(ServiceProvider.GetService<MainPage>());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
            
        }

        protected override void OnResume()
        {
            _onResumeCheckerMediator.OnResume();
        }
    }
}
