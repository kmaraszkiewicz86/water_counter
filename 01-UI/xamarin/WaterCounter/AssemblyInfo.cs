using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

[assembly: ExportFont("FontAwesomeReqular.otf", Alias = "FARegular")]
[assembly: ExportFont("FontAwesomeSolid.otf", Alias = "FASolid")]