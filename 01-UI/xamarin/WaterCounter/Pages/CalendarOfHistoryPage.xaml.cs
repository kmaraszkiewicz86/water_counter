﻿using WaterCounter.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WaterCounter.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalendarOfHistoryPage : ContentPage
    {
        public CalendarOfHistoryPage(CalendarOfHistoryViewModel calendarOfHistoryViewModel)
        {
            InitializeComponent();

            BindingContext = calendarOfHistoryViewModel;
        }
    }
}