﻿using WaterCounter.ViewModels;
using Xamarin.Forms;

namespace WaterCounter
{
    public partial class MainPage : ContentPage
    {
        public MainPage(MainPageViewModel mainPageViewModel)
        {
            InitializeComponent();

            BindingContext = mainPageViewModel;
        }

        void CarouselView_Scrolled(System.Object sender, Xamarin.Forms.ItemsViewScrolledEventArgs e)
        {
        }
    }
}