﻿using System;
using System.Globalization;
using WaterCounter.Extension;
using Xamarin.Forms;

namespace WaterCounter.Converters
{
    public class StringPathToImageSourceConventer : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string source))
            {
                return null;
            }

            return source.ToImageSource();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}