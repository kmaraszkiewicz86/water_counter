﻿namespace WaterCounter.Enums
{
    public enum NavigationPageType
    {
        None,
        MainPage,
        SettingsPage,
        CalendarOfHistory
    }
}