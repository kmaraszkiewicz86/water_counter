//
//  WaterCounterApp.swift
//  WaterCounter
//
//  Created by USER on 07/09/2021.
//

import SwiftUI

@main
struct WaterCounterApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
