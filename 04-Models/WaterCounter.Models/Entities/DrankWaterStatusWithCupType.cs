﻿using System;

namespace WaterCounter.Models.Entities
{
    public class DrankWaterStatusWithCupType : Entity
    {
        public DateTime AddedDate { get; set; }

        public int CupTypeId { get; set; }

        public string Name { get; set; }

        public int Value { get; set; }
    }
}