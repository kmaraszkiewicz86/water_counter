﻿using SQLite;

namespace WaterCounter.Models.Entities
{
    [Table("CupType")]
    public class CupType : Entity
    {
        [NotNull, Unique]
        public string Name { get; set; }

        [NotNull, Unique]
        public int Value { get; set; }
    }
}