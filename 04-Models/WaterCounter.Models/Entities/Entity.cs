﻿using SQLite;

namespace WaterCounter.Models.Entities
{
    public class Entity
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }
    }
}