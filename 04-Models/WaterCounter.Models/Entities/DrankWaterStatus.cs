﻿using System;
using SQLite;

namespace WaterCounter.Models.Entities
{
    [Table("DrankWaterStatus")]
    public class DrankWaterStatus : Entity
    {
        [NotNull]
        public int CupTypeId { get; set; }

        [NotNull]
        public DateTime AddedDate { get; set; }
    }
}