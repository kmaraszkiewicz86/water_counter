﻿using System;
using System.Collections.Generic;
using System.Linq;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Models;

namespace WaterCounter.Models.Extensions
{
    public static class DrankWaterStatusReportModelExtension
    {
        public static DrankWaterStatusReportModel ToDrankWaterStatusReportModel(this DrankWaterStatus drankWaterStatus, CupType cupType)
        {
            return new DrankWaterStatusReportModel(drankWaterStatus.Id.Value, cupType.ToCupTypeModel());
        }

        public static DrankWaterStatusReportModel ToDrankWaterDailyStatusModel(this Tuple<int, CupType> cupType)
        {
            return new DrankWaterStatusReportModel(cupType.Item1, cupType.Item2.ToCupTypeModel());
        }

        public static IEnumerable<DrankWaterStatusReportModel> ToDrankWaterDailyStatusModels(this IEnumerable<Tuple<int, CupType>> cupTypes)
        {
            return cupTypes?.Select(c => c.ToDrankWaterDailyStatusModel()) ?? new List<DrankWaterStatusReportModel>();
        }
    }
}