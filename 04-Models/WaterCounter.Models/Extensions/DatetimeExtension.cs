﻿using System;

namespace WaterCounter.Models.Extensions
{
    public static class DatetimeExtension
    {
        public static bool IsDateToday(this DateTime dateTime)
        {
            var currentDate = DateTime.Now;

            return dateTime.Year == currentDate.Year
                && dateTime.Month == currentDate.Month
                && dateTime.Day == currentDate.Day;
        }

        public static bool IsNewReportDateGreaterThenToday(this DateTime newDateForReport)
        {
            return newDateForReport > DateTime.Now;
        }

        public static bool IsNewReportHasLessThanThirtyDaysBack(this DateTime newDateForReport)
        {
            var minimumDateTime = DateTime.Now.AddDays(-30);

            var minimumDateWitoutTime = new DateTime(minimumDateTime.Year, minimumDateTime.Month, minimumDateTime.Day);
            var todayWithoutTime = new DateTime(newDateForReport.Year, newDateForReport.Month, newDateForReport.Day);

            return minimumDateWitoutTime > todayWithoutTime;
        }

        public static DateTime ReturnValidDate(this DateTime newDateForReport)
        {
            if (newDateForReport.IsNewReportDateGreaterThenToday())
            {
                return DateTime.Now;
            }
            else if (newDateForReport.IsNewReportHasLessThanThirtyDaysBack())
            {
                return DateTime.Now.AddDays(-30);
            }

            return newDateForReport;
        }
    }
}