﻿using System.Collections.Generic;
using System.Linq;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Models;

namespace WaterCounter.Models.Extensions
{
    public static class CupTypeExtension
    {
        public static CupTypeModel ToCupTypeModel(this CupType cupType)
        {
            return new CupTypeModel(cupType.Id.Value, cupType.Name, cupType.Value);
        }

        public static IEnumerable<CupTypeModel> ToCupTypeModels(this IEnumerable<CupType> cupTypes)
        {
            return cupTypes?.Select(c => c.ToCupTypeModel()) ?? new List<CupTypeModel>();
        }
    }
}