﻿namespace WaterCounter.Models.Enums
{
    public enum DrankReportStatusByDateCriteria
    {
        PreviousDate,
        NextDate
    }
}