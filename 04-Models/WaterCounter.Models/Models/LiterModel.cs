﻿namespace WaterCounter.Models.Models
{
    public class LiterModel
    {
        public int Liters { get; private set; }

        public int Mililiters { get; private set; }

        public int TotalMiliters
        {
            get
            {
                var totalMiliters = Mililiters;
                if (Liters > 0)
                {
                    totalMiliters += Liters * 1000;
                }

                return totalMiliters;
            }
        }

        public LiterModel(int liters, int mililiters)
        {
            Liters = liters;
            Mililiters = mililiters;
        }

        public void Substract(int militeres)
        {
            if (militeres <= 0)
                return;

            var totalMiliters = TotalMiliters;

            totalMiliters -= militeres;

            CalcaulateModelValues(totalMiliters);
        }

        public void Add(int militeres)
        {
            if (militeres <= 0)
                return;

            var totalMiliters = TotalMiliters;

            totalMiliters += militeres;

            CalcaulateModelValues(totalMiliters);
        }

        private void CalcaulateModelValues(int totalMiliters)
        {
            Liters = 0;
            Mililiters = 0;
            if (totalMiliters < 0)
            {
                return;
            }

            Mililiters = totalMiliters;

            if (totalMiliters >= 1000)
            {
                Liters = totalMiliters / 1000;
            }

            if (Liters > 0)
            {
                Mililiters = totalMiliters - (Liters * 1000);
            }
        }
    }
}