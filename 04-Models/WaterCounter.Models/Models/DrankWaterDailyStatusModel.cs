﻿using System;
using System.Collections.Generic;

namespace WaterCounter.Models.Models
{
    public class DrankWaterDailyStatusModelCollection
    {
        public DrankWaterDailyStatusModelCollection(IEnumerable<DrankWaterStatusReportModel> drankWaterStatusReportModels,
            DateTime currentDate,
            int cupTypeValuesSummaryInMililiters,
            LiterModel cupTypeValuesSummaryInLiters)
        {
            DrankWaterStatusReportModels = drankWaterStatusReportModels;
            CurrentDate = currentDate;
            CupTypeValuesSummaryInMililiters = cupTypeValuesSummaryInMililiters;
            LiterModelSummary = cupTypeValuesSummaryInLiters;
        }

        public IEnumerable<DrankWaterStatusReportModel> DrankWaterStatusReportModels { get; }

        public DateTime CurrentDate { get; }

        public int CupTypeValuesSummaryInMililiters { get; }

        public LiterModel LiterModelSummary { get; }

    }
}