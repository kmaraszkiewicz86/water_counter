﻿namespace WaterCounter.Models.Models
{
    public class CupTypeModel
    {
        public int Id { get; }

        public string Name { get; }

        public int Value { get; }

        public string ImagePath => "WaterCounter.Images.water-glass.png";

        public CupTypeModel(string name, int value)
        {
            Name = name;
            Value = value;
        }

        public CupTypeModel(int id, string name, int value) : this(name, value)
        {
            Id = id;
        }
    }
}