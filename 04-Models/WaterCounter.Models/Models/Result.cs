﻿namespace WaterCounter.Models.Models
{
    public class Result
    {
        public bool IsValid { get; }
        public string ErrorMessage { get; }

        public Result() : this(true)
        {
        }

        public Result(bool isValid)
        {
            IsValid = isValid;
        }

        public Result(string errorMessage) : this(false)
        {
            ErrorMessage = errorMessage;
        }

        public static Result Ok() => new Result();

        public static ResultWithModel<TModel> Ok<TModel>(TModel model) => new ResultWithModel<TModel>(model);

        public static Result Fail(string errorMessage) => new Result(errorMessage);

        public static Result Fail() => new Result(false);

        public static ResultWithModel<TModel> FailWithModel<TModel>() => new ResultWithModel<TModel>(false);

        public static ResultWithModel<TModel> FailWithModel<TModel>(string errorMessage) => new ResultWithModel<TModel>(errorMessage);
    }
}