﻿namespace WaterCounter.Models.Models
{
    public class ResultWithModel<TModel> : Result
    {
        public bool IsModelNull => Model == null;

        public TModel Model { get; } = default;

        public ResultWithModel(TModel model)
            : base(true)
        {
            Model = model;
        }

        public ResultWithModel(bool isValid) : base(isValid)
        {
            Model = default(TModel);
        }

        public ResultWithModel(string errorMessage) : base(errorMessage)
        {

        }
    }
}