﻿namespace WaterCounter.Models.Models
{
    public class DrankWaterStatusReportModel
    {
        public int Id { get; }

        public int Value => CupTypeModel.Value;

        public string ImagePath => CupTypeModel.ImagePath;

        public CupTypeModel CupTypeModel { get; }

        public DrankWaterStatusReportModel(int id, CupTypeModel cupTypeModel)
        {
            Id = id;
            CupTypeModel = cupTypeModel;
        }
    }
}