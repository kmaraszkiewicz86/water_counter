﻿using System;

namespace WaterCounter.Models.Models
{
    public class DrankWaterReportByDayModel
    {
        public int MinimumWaterPlannedToDrunk => 3000;

        public bool IsMinimalDrankWaterExceded => DrankWaterInMililiters >= MinimumWaterPlannedToDrunk;

        public string DrankWaterCountDescription
        {
            get
            {
                if (DrankWaterInMililiters == 0)
                    return $"Brak wypitych szklanek wody.";

                return $"Ilość wypitej wody {DrankWaterInMililiters} ml";
            }
        }

        public int DrankWaterInMililiters { get; set; }

        public string Day { get; set; }

        public DrankWaterReportByDayModel()
        {

        }

        public DrankWaterReportByDayModel(int drankWaterInMililiters, string day)
        {
            DrankWaterInMililiters = drankWaterInMililiters;
            Day = day;
        }
    }
}