﻿using System;
using WaterCounter.Service.Services.Interfaces.Queries;

namespace WaterCounter.Service.Services.Implementations.Queries
{
    public class DrankWaterDailyStatusQuery : IQuery
    {
        public DateTime ForDay { get; }

        public DrankWaterDailyStatusQuery(DateTime forDay)
        {
            ForDay = forDay;
        }
    }
}