﻿using System.Collections.Generic;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.Commands;

namespace WaterCounter.Service.Services.Implementations.Commands
{
    public class AddCupTypeCommand : IServiceCommand
    {
        public IEnumerable<CupTypeModel> CupTypeModels { get; }

        public AddCupTypeCommand(IEnumerable<CupTypeModel> cupTypeModel)
        {
            CupTypeModels = cupTypeModel;
        }
    }
}