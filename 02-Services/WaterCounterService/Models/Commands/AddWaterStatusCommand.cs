﻿using System;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.Commands;

namespace WaterCounter.Service.Services.Implementations.Commands
{
    public class AddDrankWaterStatusCommand : IServiceCommand
    {
        public CupTypeModel CupTypeModel { get; }

        public DateTime AddedDateTime { get; }

        public AddDrankWaterStatusCommand(CupTypeModel cupTypeModel, 
            DateTime addedDateTime)
        {
            CupTypeModel = cupTypeModel;
            AddedDateTime = addedDateTime;
        }
    }
}