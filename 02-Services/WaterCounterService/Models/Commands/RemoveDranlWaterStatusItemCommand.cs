﻿using WaterCounter.Service.Services.Interfaces.Commands;

namespace WaterCounter.Service.Services.Implementations.Commands
{
    public class RemoveDranlWaterStatusItemCommand : IServiceCommand
    {
        public int Id { get; set; }
        
        public RemoveDranlWaterStatusItemCommand(int id)
        {
            Id = id;
        }
    }
}