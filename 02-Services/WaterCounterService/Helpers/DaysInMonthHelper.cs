﻿using System;
using System.Collections.Generic;

namespace WaterCounter.Service.Helpers
{
    public class DaysInMonthHelper
    {
        private static Dictionary<int, int> _daysInMonths = new Dictionary<int, int>
        {
            { 1, 31 },
            { 3, 31 },
            { 4, 30 },
            { 5, 31 },
            { 6, 30 },
            { 7, 31 },
            { 8, 31 },
            { 9, 30 },
            { 10, 31 },
            { 11, 30 },
            { 12, 31 }
        };

        public static int GetDaysOfMonth(DateTime currentDate)
        {
            if (currentDate.Month == 2)
            {
                return currentDate.Year % 2 == 0 ? 29 : 28;
            }

            return _daysInMonths[currentDate.Month];
        }
    }
}