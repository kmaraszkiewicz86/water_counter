﻿using WaterCounter.Service.Services.Interfaces.CommandHandlers;

namespace WaterCounter.Service.Services.Interfaces.Decorators
{
    public interface IRemoveDrankWaterStatusCommandHandlerDecoratorAsync : IRemoveDrankWaterStatusCommandHandlerAsync
    {
    }
}