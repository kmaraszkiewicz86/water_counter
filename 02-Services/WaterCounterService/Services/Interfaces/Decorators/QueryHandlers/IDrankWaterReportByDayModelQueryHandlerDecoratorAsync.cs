﻿using System.Collections.Generic;
using WaterCounter.Models.Models;

namespace WaterCounter.Service.Services.Interfaces.Decorators
{
    public interface IDrankWaterReportByDayModelQueryHandlerDecoratorAsync : IQueryHandlerDecoratorAsync<IEnumerable<DrankWaterReportByDayModel>>
    {

    }
}