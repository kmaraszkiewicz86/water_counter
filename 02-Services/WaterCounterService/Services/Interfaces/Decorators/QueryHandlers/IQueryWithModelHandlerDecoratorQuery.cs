﻿using System.Threading.Tasks;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.Queries;

namespace WaterCounter.Service.Services.Interfaces.Decorators
{
    public interface IQueryWithModelHandlerDecoratorQuery<TQuery, TResultModel>
        where TQuery : IQuery
    {
        Task<ResultWithModel<TResultModel>> HandleAsync(TQuery query);
    }
}