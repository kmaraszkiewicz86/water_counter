﻿using System.Collections.Generic;
using WaterCounter.Models.Models;

namespace WaterCounter.Service.Services.Interfaces.Decorators
{
    public interface ICupTypesQueryHandlerDecoratorAsync : IQueryHandlerDecoratorAsync<IEnumerable<CupTypeModel>>
    {

    }
}