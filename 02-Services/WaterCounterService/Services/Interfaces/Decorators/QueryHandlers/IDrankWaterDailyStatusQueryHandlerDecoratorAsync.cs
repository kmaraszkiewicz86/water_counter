﻿using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Implementations.Queries;

namespace WaterCounter.Service.Services.Interfaces.Decorators
{
    public interface IDrankWaterDailyStatusQueryHandlerDecorator : IQueryWithModelHandlerDecoratorQuery<DrankWaterDailyStatusQuery, DrankWaterDailyStatusModelCollection>
    {
    }
}