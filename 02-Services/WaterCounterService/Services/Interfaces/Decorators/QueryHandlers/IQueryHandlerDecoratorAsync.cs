﻿using System.Threading.Tasks;
using WaterCounter.Models.Models;

namespace WaterCounter.Service.Services.Interfaces.Decorators
{
    public interface IQueryHandlerDecoratorAsync<TResultModel>
    {
        Task<ResultWithModel<TResultModel>> HandleAsync();
    }
}