﻿using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Implementations.Queries;

namespace WaterCounter.Service.Services.Interfaces.QueryHandlers
{
    public interface IDrankWaterDailyStatusQueryHandler : IQueryHandlerWithQueryAsync<DrankWaterDailyStatusQuery, DrankWaterDailyStatusModelCollection>
    {
    }
}