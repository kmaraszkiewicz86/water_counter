﻿using System.Collections.Generic;
using WaterCounter.Models.Models;

namespace WaterCounter.Service.Services.Interfaces.QueryHandlers
{
    public interface IDrankWaterReportByDayModelQueryHandler : IQueryHandlerAsync<IEnumerable<DrankWaterReportByDayModel>>
    {
    }
}