﻿using System.Threading.Tasks;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.Queries;

namespace WaterCounter.Service.Services.Interfaces.QueryHandlers
{
    public interface IQueryHandlerWithQueryAsync<TQuery, TResult>
        where TQuery : IQuery
    {
        Task<ResultWithModel<TResult>> HandleAsync(TQuery query);
    }
}