﻿using System.Threading.Tasks;
using WaterCounter.Models.Models;

namespace WaterCounter.Service.Services.Interfaces.QueryHandlers
{
    public interface IQueryHandlerAsync<TResultModel>
    {
        Task<ResultWithModel<TResultModel>> HandleAsync();
    }
}