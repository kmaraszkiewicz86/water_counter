﻿using System.Threading.Tasks;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.Commands;

namespace WaterCounter.Service.Services.Interfaces.CommandHandlers
{
    public interface ICommandHandlerWithResultAsync<TServiceCommand, TResult>
        where TServiceCommand : IServiceCommand
    {
        Task<ResultWithModel<TResult>> ExecuteAsync(TServiceCommand serviceCommand);
    }
}