﻿using WaterCounter.Service.Services.Implementations.Commands;

namespace WaterCounter.Service.Services.Interfaces.CommandHandlers
{
    public interface ICupTypeCommandHandlerAsync : ICommandHandlerAsync<AddCupTypeCommand>
    {
    }
}