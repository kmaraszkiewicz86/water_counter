﻿using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Implementations.Commands;

namespace WaterCounter.Service.Services.Interfaces.CommandHandlers
{
    public interface IAddDrankWaterStatusCommandWithResultHandlerAsync : ICommandHandlerWithResultAsync<AddDrankWaterStatusCommand, DrankWaterStatusReportModel>
    {
    }
}