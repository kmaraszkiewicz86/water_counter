﻿using System.Threading.Tasks;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.Commands;

namespace WaterCounter.Service.Services.Interfaces.CommandHandlers
{
    public interface ICommandHandlerAsync<TServiceCommand>
        where TServiceCommand: IServiceCommand
    {
        Task<Result> ExecuteAsync(TServiceCommand serviceCommand);
    }
}