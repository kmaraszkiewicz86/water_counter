﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Extensions;
using WaterCounter.Models.Models;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Services.Implementations.Queries;
using WaterCounter.Service.Services.Interfaces.QueryHandlers;

namespace WaterCounter.Service.Services.Implementations.QueryHandlers
{
    public class DrankWaterDailyStatusQueryHandler : IDrankWaterDailyStatusQueryHandler
    {
        private readonly IRepository<CupType> _cupTypeRepository;

        private readonly IRepository<DrankWaterStatus> _drankWaterStatusRepository;

        public DrankWaterDailyStatusQueryHandler(IRepository<CupType> cupTypeRepository,
            IRepository<DrankWaterStatus> drankWaterStatusRepository)
        {
            _cupTypeRepository = cupTypeRepository;
            _drankWaterStatusRepository = drankWaterStatusRepository;
        }

        public async Task<ResultWithModel<DrankWaterDailyStatusModelCollection>> HandleAsync(DrankWaterDailyStatusQuery query)
        {
            int cupTypeValuesSummaryInMililiters = 0;
            int liters = 0;
            int mililiters = 0;

            var minDateTime = new DateTime(query.ForDay.Year, query.ForDay.Month, query.ForDay.Day, 0, 0, 0).Ticks;
            var maxDateTime = new DateTime(query.ForDay.Year, query.ForDay.Month, query.ForDay.Day, 23, 59, 59).Ticks;

            var sqlQuery = @"
                SELECT
                    d.Id,
                    d.AddedDate,
                    d.CupTypeId,
                    c.Name,
                    c.Value
                FROM
                    DrankWaterStatus AS d
                    INNER JOIN
                    CupType AS c ON c.Id = d.CupTypeId
                WHERE
                    AddedDate >= ? AND AddedDate <= ?";

            List<DrankWaterStatusWithCupType> drankWaterStatusWithCupTypes = await _drankWaterStatusRepository
                .DatabaseContext
                .Database
                .QueryAsync<DrankWaterStatusWithCupType>(sqlQuery, minDateTime, maxDateTime);

            if (drankWaterStatusWithCupTypes == null || drankWaterStatusWithCupTypes.Count == 0)
                return new ResultWithModel<DrankWaterDailyStatusModelCollection>(true);

            List<Tuple<int, CupType>> cupTypes = new List<Tuple<int, CupType>>();
            foreach (DrankWaterStatusWithCupType drankWaterStatusWithCupType in drankWaterStatusWithCupTypes)
            {
                cupTypes.Add(Tuple.Create(drankWaterStatusWithCupType.Id.Value,
                    new CupType
                    {
                        Id = drankWaterStatusWithCupType.CupTypeId,
                        Name = drankWaterStatusWithCupType.Name,
                        Value = drankWaterStatusWithCupType.Value
                    }));
            }

            if (cupTypes != null && cupTypes.Count > 0)
            {
                cupTypeValuesSummaryInMililiters = cupTypes.Sum(c => c.Item2.Value);
                liters = cupTypeValuesSummaryInMililiters / 1000;
                mililiters = cupTypeValuesSummaryInMililiters - (liters * 1000);
            }

            return Result.Ok(new DrankWaterDailyStatusModelCollection(
                cupTypes.ToDrankWaterDailyStatusModels(),
                query.ForDay,
                cupTypeValuesSummaryInMililiters,
                new LiterModel(liters, mililiters)));
        }
    }
}