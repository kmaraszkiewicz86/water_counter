﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Extensions;
using WaterCounter.Models.Models;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Services.Interfaces.QueryHandlers;

namespace WaterCounter.Service.Services.Implementations.QueryHandlers
{
    public class CupTypesQueryHandlerAsync : DatabaseBase<CupType>, ICupTypesQueryHandlerAsync
    {
        public CupTypesQueryHandlerAsync(IRepository<CupType> repository) : base(repository)
        {
        }

        public async Task<ResultWithModel<IEnumerable<CupTypeModel>>> HandleAsync()
        {
            IEnumerable<CupType> cupTypes = await _repository.GetItemsAsync();

            cupTypes = cupTypes.OrderBy(c => c.Value);

            return Result.Ok(cupTypes.ToCupTypeModels());
        }
    }
}