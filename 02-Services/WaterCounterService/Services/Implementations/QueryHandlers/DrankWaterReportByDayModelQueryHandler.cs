﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Models;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Services.Interfaces.QueryHandlers;

namespace WaterCounter.Service.Services.Implementations.QueryHandlers
{
    public class DrankWaterReportByDayModelQueryHandler : IDrankWaterReportByDayModelQueryHandler
    {
        private readonly IRepository<DrankWaterStatus> _drankWaterStatusRepository;

        public DrankWaterReportByDayModelQueryHandler(IRepository<DrankWaterStatus> drankWaterStatusRepository)
        {
            _drankWaterStatusRepository = drankWaterStatusRepository;
        }

        public async Task<ResultWithModel<IEnumerable<DrankWaterReportByDayModel>>> HandleAsync()
        {
            DateTime minDateTime = DateTime.UtcNow.AddDays(-31);
            DateTime maxDateTime = DateTime.UtcNow.AddDays(-1);

            long minDateTimeTicks = new DateTime(minDateTime.Year, minDateTime.Month, minDateTime.Day, 0, 0, 0).Ticks;
            long maxDateTimeTicks = new DateTime(maxDateTime.Year, maxDateTime.Month, maxDateTime.Day, 23, 59, 59).Ticks;

            var sqlQuery = @"SELECT 
	                            SUM(c.Value) AS DrankWaterInMililiters,
	                            DATE(DATETIME(AddedDate/10000000 - 62135596800, 'unixepoch')) AS Day
                            FROM 
	                            DrankWaterStatus AS d
	                            JOIN
	                            CupType AS c ON c.Id = d.CupTypeId
                            WHERE
                                AddedDate >= ? AND AddedDate <= ?
                            GROUP BY DATE(DATETIME(AddedDate/10000000 - 62135596800, 'unixepoch'))
                            ORDER BY
	                            DATE(DATETIME(AddedDate/10000000 - 62135596800, 'unixepoch')) DESC";

            List<DrankWaterReportByDayModel> drankWaterStatusWithCupTypesFromDb = await _drankWaterStatusRepository
                .DatabaseContext
                .Database
                .QueryAsync<DrankWaterReportByDayModel>(sqlQuery, minDateTimeTicks, maxDateTimeTicks);

            return new ResultWithModel<IEnumerable<DrankWaterReportByDayModel>>(GenerateReportFromRecentMonth(drankWaterStatusWithCupTypesFromDb));
        }

        private List<DrankWaterReportByDayModel> GenerateReportFromRecentMonth(List<DrankWaterReportByDayModel> drankWaterStatusWithCupTypesFromDb)
        {
            List<DrankWaterReportByDayModel> drankWaterStatusFromWholeMonth = new List<DrankWaterReportByDayModel>();

            for (var daysBeforeCount = 1; daysBeforeCount <= 31; daysBeforeCount++)
            {
                var dayForCurrentDay = DateTime.Now.AddDays(-daysBeforeCount);
                var dateFromXDaysBeforeToday = dayForCurrentDay.ToString("yyyy-MM-dd");

                var reportFromXDay = drankWaterStatusWithCupTypesFromDb.FirstOrDefault(d => d.Day == dateFromXDaysBeforeToday);

                if (reportFromXDay == null)
                {
                    drankWaterStatusFromWholeMonth.Add(new DrankWaterReportByDayModel
                    {
                        Day = dayForCurrentDay.ToString("dd-MM"),
                        DrankWaterInMililiters = 0
                    });
                }
                else
                {
                    drankWaterStatusFromWholeMonth.Add(new DrankWaterReportByDayModel
                    {
                        Day = dayForCurrentDay.ToString("dd-MM"),
                        DrankWaterInMililiters = reportFromXDay.DrankWaterInMililiters
                    });
                }
            }

            return drankWaterStatusFromWholeMonth;
        }
    }
}