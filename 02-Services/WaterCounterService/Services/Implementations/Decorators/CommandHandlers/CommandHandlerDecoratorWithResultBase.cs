﻿using System.Threading.Tasks;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.CommandHandlers;
using WaterCounter.Service.Services.Interfaces.Commands;

namespace WaterCounter.Service.Services.Implementations.Decorators
{
    public abstract class CommandHandlerDecoratorWithResultBase<TCommandHandler, TServiceCommand, TResult>
        where TServiceCommand : IServiceCommand
        where TCommandHandler : ICommandHandlerWithResultAsync<TServiceCommand, TResult>
    {
        private readonly ICommandHandlerWithResultAsync<TServiceCommand, TResult> _commandHandlerAsync;

        protected CommandHandlerDecoratorWithResultBase(ICommandHandlerWithResultAsync<TServiceCommand, TResult> commandHandlerAsync)
        {
            _commandHandlerAsync = commandHandlerAsync;
        }

        public async Task<ResultWithModel<TResult>> ExecuteAsync(TServiceCommand serviceCommand)
        {
            if (serviceCommand == null)
            {
                return Result.FailWithModel<TResult>($"The model {serviceCommand.GetType()} could not be empty!");
            }

            try
            {
                return await _commandHandlerAsync.ExecuteAsync(serviceCommand);
            }
            catch
            {
                return Result.FailWithModel<TResult>("The unknown error occours when trying to modify database");
            }
        }
    }
}