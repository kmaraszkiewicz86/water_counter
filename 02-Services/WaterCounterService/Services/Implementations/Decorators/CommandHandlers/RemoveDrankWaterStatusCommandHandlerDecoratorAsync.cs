﻿using WaterCounter.Service.Services.Implementations.CommandHandlers;
using WaterCounter.Service.Services.Implementations.Commands;
using WaterCounter.Service.Services.Interfaces.CommandHandlers;
using WaterCounter.Service.Services.Interfaces.Decorators;

namespace WaterCounter.Service.Services.Implementations.Decorators
{
    public class RemoveDrankWaterStatusCommandHandlerDecoratorAsync : CommandHandlerDecoratorBase<RemoveDrankWaterStatusCommandHandlerAsync, RemoveDranlWaterStatusItemCommand>, IRemoveDrankWaterStatusCommandHandlerDecoratorAsync
    {
        public RemoveDrankWaterStatusCommandHandlerDecoratorAsync(IRemoveDrankWaterStatusCommandHandlerAsync commandHandlerAsync) : base(commandHandlerAsync)
        {
        }
    }
}