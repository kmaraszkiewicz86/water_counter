﻿using System;
using System.Threading.Tasks;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.CommandHandlers;
using WaterCounter.Service.Services.Interfaces.Commands;

namespace WaterCounter.Service.Services.Implementations.Decorators
{
    public abstract class CommandHandlerDecoratorBase<TCommandHandler, TServiceCommand>
        where TServiceCommand : IServiceCommand
        where TCommandHandler : ICommandHandlerAsync<TServiceCommand>
    {
        private readonly ICommandHandlerAsync<TServiceCommand> _commandHandlerAsync;

        protected CommandHandlerDecoratorBase(ICommandHandlerAsync<TServiceCommand> commandHandlerAsync)
        {
            _commandHandlerAsync = commandHandlerAsync;
        }

        public async Task<Result> ExecuteAsync(TServiceCommand serviceCommand)
        {
            if (serviceCommand == null)
            {
                return Result.Fail($"The model {serviceCommand.GetType()} could not be empty!");
            }

            try
            {
                return await _commandHandlerAsync.ExecuteAsync(serviceCommand);
            }
            catch
            {
                return Result.Fail("The unknown error occours when trying to modify database");
            }
        }
    }
}