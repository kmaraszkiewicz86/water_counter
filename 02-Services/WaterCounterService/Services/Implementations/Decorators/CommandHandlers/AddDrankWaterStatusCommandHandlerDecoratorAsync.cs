﻿using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Implementations.CommandHandlers;
using WaterCounter.Service.Services.Implementations.Commands;
using WaterCounter.Service.Services.Interfaces.CommandHandlers;
using WaterCounter.Service.Services.Interfaces.Decorators;

namespace WaterCounter.Service.Services.Implementations.Decorators
{
    public class AddDrankWaterStatusCommandHandlerDecoratorAsync : CommandHandlerDecoratorWithResultBase<AddDrankWaterStatusCommandHandlerAsync, AddDrankWaterStatusCommand, DrankWaterStatusReportModel>, IAddDrankWaterStatusCommandHandlerDecoratorAsync
    {
        public AddDrankWaterStatusCommandHandlerDecoratorAsync(IAddDrankWaterStatusCommandWithResultHandlerAsync commandHandlerAsync) : base(commandHandlerAsync)
        {
        }
    }
}