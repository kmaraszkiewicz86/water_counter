﻿using System.Threading.Tasks;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.Decorators;
using WaterCounter.Service.Services.Interfaces.QueryHandlers;

namespace WaterCounter.Service.Services.Implementations.Decorators
{
    public abstract class QueryHandlerDecoratorBaseAsync<TResultModel> : IQueryHandlerDecoratorAsync<TResultModel>
    {
        private readonly IQueryHandlerAsync<TResultModel> _queryHandlerAsync;

        public QueryHandlerDecoratorBaseAsync(IQueryHandlerAsync<TResultModel> queryHandlerAsync)
        {
            _queryHandlerAsync = queryHandlerAsync;
        }

        public async Task<ResultWithModel<TResultModel>> HandleAsync()
        {
            try
            {
                return await _queryHandlerAsync.HandleAsync();
            }
            catch
            {
                return Result.FailWithModel<TResultModel>("Unknown error occours"); 
            }
        }
    }
}