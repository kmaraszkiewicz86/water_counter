﻿using System.Collections.Generic;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.Decorators;
using WaterCounter.Service.Services.Interfaces.QueryHandlers;

namespace WaterCounter.Service.Services.Implementations.Decorators
{
    public class DrankWaterReportByDayModelQueryHandlerDecoratorAsync : QueryHandlerDecoratorBaseAsync<IEnumerable<DrankWaterReportByDayModel>>, IDrankWaterReportByDayModelQueryHandlerDecoratorAsync
    {
        public DrankWaterReportByDayModelQueryHandlerDecoratorAsync(IDrankWaterReportByDayModelQueryHandler queryHandlerAsync) : base(queryHandlerAsync)
        {
        }
    }
}