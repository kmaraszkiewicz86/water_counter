﻿using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Implementations.Queries;
using WaterCounter.Service.Services.Interfaces.Decorators;
using WaterCounter.Service.Services.Interfaces.QueryHandlers;

namespace WaterCounter.Service.Services.Implementations.Decorators
{
    public class DrankWaterDailyStatusQueryHandlerDecorator : QueryWithModelHandlerDecoratorQueryBase<DrankWaterDailyStatusQuery, DrankWaterDailyStatusModelCollection>, IDrankWaterDailyStatusQueryHandlerDecorator
    {
        public DrankWaterDailyStatusQueryHandlerDecorator(IDrankWaterDailyStatusQueryHandler queryHandlerAsync) : base(queryHandlerAsync)
        {
        }
    }
}
