﻿using System.Collections.Generic;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.Decorators;
using WaterCounter.Service.Services.Interfaces.QueryHandlers;

namespace WaterCounter.Service.Services.Implementations.Decorators
{
    public class CupTypesQueryHandlerDecoratorAsync : QueryHandlerDecoratorBaseAsync<IEnumerable<CupTypeModel>>, ICupTypesQueryHandlerDecoratorAsync
    {
        public CupTypesQueryHandlerDecoratorAsync(ICupTypesQueryHandlerAsync queryHandlerAsync) : base(queryHandlerAsync)
        {
        }
    }
}