﻿using System.Threading.Tasks;
using WaterCounter.Models.Models;
using WaterCounter.Service.Services.Interfaces.Decorators;
using WaterCounter.Service.Services.Interfaces.Queries;
using WaterCounter.Service.Services.Interfaces.QueryHandlers;

namespace WaterCounter.Service.Services.Implementations.Decorators
{
    public abstract class QueryWithModelHandlerDecoratorQueryBase<TQuery, TResultModel> : IQueryWithModelHandlerDecoratorQuery<TQuery, TResultModel>
        where TQuery : IQuery
    {
        private readonly IQueryHandlerWithQueryAsync<TQuery, TResultModel> _queryHandlerAsync;

        public QueryWithModelHandlerDecoratorQueryBase(IQueryHandlerWithQueryAsync<TQuery, TResultModel> queryHandlerAsync)
        {
            _queryHandlerAsync = queryHandlerAsync;
        }

        public async Task<ResultWithModel<TResultModel>> HandleAsync(TQuery query)
        {
            try
            {
                return await _queryHandlerAsync.HandleAsync(query);
            }
            catch
            {
                return Result.FailWithModel<TResultModel>("Unknown error occours");
            }
        }
    }
}
