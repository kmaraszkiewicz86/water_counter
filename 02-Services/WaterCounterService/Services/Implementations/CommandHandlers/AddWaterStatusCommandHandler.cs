﻿using System;
using System.Threading.Tasks;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Extensions;
using WaterCounter.Models.Models;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Services.Implementations.Commands;
using WaterCounter.Service.Services.Interfaces.CommandHandlers;

namespace WaterCounter.Service.Services.Implementations.CommandHandlers
{
    public class AddDrankWaterStatusCommandHandlerAsync : IAddDrankWaterStatusCommandWithResultHandlerAsync
    {
        protected readonly IRepository<CupType> _cupTypeRepository;

        protected readonly IRepository<DrankWaterStatus> _drankWaterStatusRepository;

        public AddDrankWaterStatusCommandHandlerAsync(IRepository<CupType> cupTypeRepository,
            IRepository<DrankWaterStatus> drankWaterStatusRepository)
        {
            _cupTypeRepository = cupTypeRepository;
            _drankWaterStatusRepository = drankWaterStatusRepository;
        }

        public async Task<ResultWithModel<DrankWaterStatusReportModel>> ExecuteAsync(AddDrankWaterStatusCommand serviceCommand)
        {
            if (serviceCommand == null || serviceCommand.CupTypeModel == null || serviceCommand.CupTypeModel.Id <= 0)
            {
                return Result.FailWithModel<DrankWaterStatusReportModel>($"Select cup type before continue...");
            }

            CupType cupType = await _cupTypeRepository.GetItemAsync(serviceCommand.CupTypeModel.Id);

            if (cupType == null)
            {
                return Result.FailWithModel<DrankWaterStatusReportModel>($"{nameof(CupType)} with id {serviceCommand.CupTypeModel.Id} not found");
            }

            var drankWaterStatus = new DrankWaterStatus
            {
                CupTypeId = cupType.Id.Value,
                AddedDate = serviceCommand.AddedDateTime
            };

            await _drankWaterStatusRepository.SaveItemAsync(drankWaterStatus);

            return Result.Ok<DrankWaterStatusReportModel>(drankWaterStatus.ToDrankWaterStatusReportModel(cupType));
        }
    }
}