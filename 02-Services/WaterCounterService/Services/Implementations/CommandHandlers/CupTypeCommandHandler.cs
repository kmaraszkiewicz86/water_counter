﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Models;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Services.Implementations.Commands;
using WaterCounter.Service.Services.Interfaces.CommandHandlers;

namespace WaterCounter.Service.Services.Implementations.CommandHandlers
{
    public class CupTypeCommandHandlerAsync : DatabaseBase<CupType>, ICupTypeCommandHandlerAsync
    {
        public CupTypeCommandHandlerAsync(IRepository<CupType> repository) : base(repository)
        {
        }

        public async Task<Result> ExecuteAsync(AddCupTypeCommand serviceCommand)
        {
            if (serviceCommand.CupTypeModels == null || serviceCommand.CupTypeModels.Count() == 0)
            {
                return Result.Fail($"Collection of cup names can't be empty");
            }

            foreach (CupTypeModel cupTypeModel in serviceCommand.CupTypeModels)
            {
                if (cupTypeModel == null || string.IsNullOrWhiteSpace(cupTypeModel.Name) || cupTypeModel.Value <= 0)
                {
                    continue;
                }

                List<CupType> cupTypes = await _repository.QueryAsync("SELECT Id, Name, Value FROM CupType WHERE lower(Name) = ?",
                    cupTypeModel.Name.ToLower());

                if (cupTypes != null && cupTypes.Count() > 0)
                {
                    continue;
                }

                await _repository.SaveItemAsync(new CupType { Name = cupTypeModel.Name, Value = cupTypeModel.Value });
            }

            return Result.Ok();
        }
    }
}