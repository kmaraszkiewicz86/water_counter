﻿using System.Threading.Tasks;
using WaterCounter.Models.Entities;
using WaterCounter.Models.Models;
using WaterCounter.Repository.DbCore;
using WaterCounter.Service.Services.Implementations.Commands;
using WaterCounter.Service.Services.Interfaces.CommandHandlers;

namespace WaterCounter.Service.Services.Implementations.CommandHandlers
{
    public class RemoveDrankWaterStatusCommandHandlerAsync : DatabaseBase<DrankWaterStatus>, IRemoveDrankWaterStatusCommandHandlerAsync
    {
        public RemoveDrankWaterStatusCommandHandlerAsync(IRepository<DrankWaterStatus> repository) : base(repository)
        {
        }

        public async Task<Result> ExecuteAsync(RemoveDranlWaterStatusItemCommand serviceCommand)
        {
            if (serviceCommand == null || serviceCommand.Id < 1)
                return Result.Fail("The id is required");

            DrankWaterStatus drankWaterStatus = await _repository.GetItemAsync(serviceCommand.Id);

            if (drankWaterStatus == null)
                return Result.Fail($"The item with {serviceCommand.Id} is not found");

            await _repository.DeleteItemAsync(drankWaterStatus);

            return Result.Ok();
        }
    }
}