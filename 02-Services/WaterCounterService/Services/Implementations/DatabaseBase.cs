﻿using WaterCounter.Models.Entities;
using WaterCounter.Repository.DbCore;

namespace WaterCounter.Service.Services.Implementations
{
    public abstract class DatabaseBase<TEntity>
        where TEntity : Entity, new()
    {
        protected readonly IRepository<TEntity> _repository;

        public DatabaseBase(IRepository<TEntity> repository)
        {
            _repository = repository;
        }
    }
}